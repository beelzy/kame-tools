#ifndef TYPES_H
#define TYPES_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <string>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
union ufloat {
    float f;
    u32 u;
};

static u16 u16_from_u8_data(u8* data, u32 index) {
    return data[index + 1] << 8 | data[index];
}

static u32 u32_from_u8_data(u8* data, u32 index) {
    return data[index + 3] << 24 | data[index + 2] << 16 | data[index + 1] << 8 | data[index];
}

#endif
