#include "bodylz.h"

#include <math.h>

/*
   static void* bodylz_build(int bannerType) {

   }
   */

RGBColors rgb_from_u8_data(u8* data, u32 index) {
    RGBColors color;
    color.R = data[index];
    color.G = data[index + 1];
    color.B = data[index + 2];
    return color;
}

RGBAColors rgba_from_u8_data(u8* data, u32 index) {
    RGBAColors color;
    color.R = data[index];
    color.G = data[index + 1];
    color.B = data[index + 2];
    color.A = data[index + 3];
    return color;
}

std::string rgb_to_string(RGBColors color) {
    char colorstring[9];
    sprintf(colorstring, "0x%06x", color.R << 16 | color.G << 8 | color.B);
    return std::string(colorstring);
}

std::string rgba_to_string(RGBAColors color) {
    char colorstring[11];
    sprintf(colorstring, "0x%08x", color.R << 24 | color.G << 16 | color.B << 8 | color.A);
    return std::string(colorstring);
}

void addRGBColor(RGBColors* color, unsigned int value) {
    color->R = (value & 0xFF0000) >> 16;
    color->G = (value & 0x00FF00) >> 8;
    color->B = (value & 0x0000FF);;
}

void addRGBAColor(RGBAColors* color, unsigned int value) {
    color->R = (value & 0xFF000000) >> 24;
    color->G = (value & 0x00FF0000) >> 16;
    color->B = (value & 0x0000FF00) >> 8;
    color->A = (value & 0x000000FF);
}

void addCursorColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* dark = config->GetValue("colors", "cursorcolor.dark");
    const char* main = config->GetValue("colors", "cursorcolor.main");
    const char* light = config->GetValue("colors", "cursorcolor.light");
    const char* glow = config->GetValue("colors", "cursorcolor.glow");

    bool cursorcolor = false;

    if (dark != NULL) {
        addRGBColor(&bodylz->colors.cursorColors[0], std::stoul(dark, nullptr, 16));
        cursorcolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.cursorColors[1], std::stoul(main, nullptr, 16));
        cursorcolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.cursorColors[2], std::stoul(light, nullptr, 16));
        cursorcolor = true;
    }
    if (glow != NULL) {
        addRGBColor(&bodylz->colors.cursorColors[3], std::stoul(glow, nullptr, 16));
        cursorcolor = true;
    }

    bodylz->flags.cursorColor = cursorcolor;
    if (cursorcolor) {
        bodylz->flags.cursorOffset = offsetof(BODYLZ, colors.cursorColors[0]);
    }
}

void addFolderColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* dark = config->GetValue("colors", "foldercolor.dark");
    const char* main = config->GetValue("colors", "foldercolor.main");
    const char* light = config->GetValue("colors", "foldercolor.light");
    const char* shadow = config->GetValue("colors", "foldercolor.shadow");

    bool foldercolor = false;

    if (dark != NULL) {
        addRGBColor(&bodylz->colors.folderColors[0], std::stoul(dark, nullptr, 16));
        foldercolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.folderColors[1], std::stoul(main, nullptr, 16));
        foldercolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.folderColors[2], std::stoul(light, nullptr, 16));
        foldercolor = true;
    }
    if (shadow != NULL) {
        addRGBColor(&bodylz->colors.folderColors[3], std::stoul(shadow, nullptr, 16));
        foldercolor = true;
    }

    bodylz->flags.folderColor = foldercolor;
    if (foldercolor) {
        bodylz->flags.folderOffset = offsetof(BODYLZ, colors.folderColors[0]);
    }
}

void addFileColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* dark = config->GetValue("colors", "filecolor.dark");
    const char* main = config->GetValue("colors", "filecolor.main");
    const char* light = config->GetValue("colors", "filecolor.light");
    const char* shadow = config->GetValue("colors", "filecolor.shadow");

    bool filecolor = false;

    if (dark != NULL) {
        addRGBColor(&bodylz->colors.fileColors[0], std::stoul(dark, nullptr, 16));
        filecolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.fileColors[1], std::stoul(main, nullptr, 16));
        filecolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.fileColors[2], std::stoul(light, nullptr, 16));
        filecolor = true;
    }
    if (shadow != NULL) {
        addRGBAColor(&bodylz->colors.fileShadow, std::stoul(shadow, nullptr, 16));
        filecolor = true;
    }

    bodylz->flags.fileColor = filecolor;
    if (filecolor) {
        bodylz->flags.fileOffset = offsetof(BODYLZ, colors.fileColors[0]);
    }
}

void addArrowButtonColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* dark = config->GetValue("colors", "arrowbuttoncolor.dark");
    const char* main = config->GetValue("colors", "arrowbuttoncolor.main");
    const char* light = config->GetValue("colors", "arrowbuttoncolor.light");
    const char* shadow = config->GetValue("colors", "arrowbuttoncolor.shadow");

    bool arrowbuttoncolor = false;

    if (dark != NULL) {
        addRGBColor(&bodylz->colors.arrowButtonColors[0], std::stoul(dark, nullptr, 16));
        arrowbuttoncolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.arrowButtonColors[1], std::stoul(main, nullptr, 16));
        arrowbuttoncolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.arrowButtonColors[2], std::stoul(light, nullptr, 16));
        arrowbuttoncolor = true;
    }
    if (shadow != NULL) {
        addRGBAColor(&bodylz->colors.arrowButtonShadow, std::stoul(shadow, nullptr, 16));
        arrowbuttoncolor = true;
    }

    bodylz->flags.arrowButtonColor = arrowbuttoncolor;
    if (arrowbuttoncolor) {
        bodylz->flags.arrowButtonOffset = offsetof(BODYLZ, colors.arrowButtonColors[0]);
    }
}

void addArrowColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* border = config->GetValue("colors", "arrowcolor.border");
    const char* pressed = config->GetValue("colors", "arrowcolor.pressed");
    const char* unpressed = config->GetValue("colors", "arrowcolor.unpressed");

    bool arrowcolor = false;

    if (border != NULL) {
        addRGBColor(&bodylz->colors.arrowColors[0], std::stoul(border, nullptr, 16));
        arrowcolor = true;
    }
    if (pressed != NULL) {
        addRGBColor(&bodylz->colors.arrowColors[2], std::stoul(pressed, nullptr, 16));
        arrowcolor = true;
    }
    if (unpressed != NULL) {
        addRGBColor(&bodylz->colors.arrowColors[1], std::stoul(unpressed, nullptr, 16));
        arrowcolor = true;
    }

    bodylz->flags.arrowColor = arrowcolor;
    if (arrowcolor) {
        bodylz->flags.arrowOffset = offsetof(BODYLZ, colors.arrowColors[0]);
    }
}

void addOpenColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* textShadowPos = config->GetValue("colors", "open.textshadowpos");
    const char* dark = config->GetValue("colors", "open.dark");
    const char* main = config->GetValue("colors", "open.main");
    const char* light = config->GetValue("colors", "open.light");
    const char* shadow = config->GetValue("colors", "open.shadow");
    const char* glow = config->GetValue("colors", "open.glow");
    const char* textshadow = config->GetValue("colors", "open.textshadow");
    const char* textmain = config->GetValue("colors", "open.textmain");
    const char* textselected = config->GetValue("colors", "open.textselected");

    bool opencolor = false;

    if (textShadowPos != NULL) {
        ufloat pos;
        pos.f = std::stof(textShadowPos) + 0.0f;
        bodylz->colors.openColors.textShadowPos = pos.u;
    }
    if (dark != NULL) {
        addRGBColor(&bodylz->colors.openColors.dark, std::stoul(dark, nullptr, 16));
        opencolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.openColors.main, std::stoul(main, nullptr, 16));
        opencolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.openColors.light, std::stoul(light, nullptr, 16));
        opencolor = true;
    }
    if (shadow != NULL) {
        addRGBAColor(&bodylz->colors.openColors.shadow, std::stoul(shadow, nullptr, 16));
        opencolor = true;
    }
    if (glow != NULL) {
        addRGBColor(&bodylz->colors.openColors.glow, std::stoul(glow, nullptr, 16));
        opencolor = true;
    }
    if (textshadow != NULL) {
        addRGBColor(&bodylz->colors.openColors.textShadow, std::stoul(textshadow, nullptr, 16));
        opencolor = true;
    }
    if (textmain != NULL) {
        addRGBColor(&bodylz->colors.openColors.textMain, std::stoul(textmain, nullptr, 16));
        opencolor = true;
    }
    if (textselected != NULL) {
        addRGBColor(&bodylz->colors.openColors.textSelected, std::stoul(textselected, nullptr, 16));
        opencolor = true;
    }

    bodylz->flags.openCloseColor = bodylz->flags.openCloseColor ? 1 : opencolor;
    if (opencolor) {
        bodylz->flags.openColorOffset = offsetof(BODYLZ, colors.openColors);
    }
}

void addCloseColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* textShadowPos = config->GetValue("colors", "close.textshadowpos");
    const char* dark = config->GetValue("colors", "close.dark");
    const char* main = config->GetValue("colors", "close.main");
    const char* light = config->GetValue("colors", "close.light");
    const char* shadow = config->GetValue("colors", "close.shadow");
    const char* glow = config->GetValue("colors", "close.glow");
    const char* textshadow = config->GetValue("colors", "close.textshadow");
    const char* textmain = config->GetValue("colors", "close.textmain");
    const char* textselected = config->GetValue("colors", "close.textselected");

    bool closecolor = false;

    if (textShadowPos != NULL) {
        ufloat pos;
        pos.f = std::stof(textShadowPos) + 0.0f;
        bodylz->colors.closeColors.textShadowPos = pos.u;
    }
    if (dark != NULL) {
        addRGBColor(&bodylz->colors.closeColors.dark, std::stoul(dark, nullptr, 16));
        closecolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.closeColors.main, std::stoul(main, nullptr, 16));
        closecolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.closeColors.light, std::stoul(light, nullptr, 16));
        closecolor = true;
    }
    if (shadow != NULL) {
        addRGBAColor(&bodylz->colors.closeColors.shadow, std::stoul(shadow, nullptr, 16));
        closecolor = true;
    }
    if (glow != NULL) {
        addRGBColor(&bodylz->colors.closeColors.glow, std::stoul(glow, nullptr, 16));
        closecolor = true;
    }
    if (textshadow != NULL) {
        addRGBColor(&bodylz->colors.closeColors.textShadow, std::stoul(textshadow, nullptr, 16));
        closecolor = true;
    }
    if (textmain != NULL) {
        addRGBColor(&bodylz->colors.closeColors.textMain, std::stoul(textmain, nullptr, 16));
        closecolor = true;
    }
    if (textselected != NULL) {
        addRGBColor(&bodylz->colors.closeColors.textSelected, std::stoul(textselected, nullptr, 16));
        closecolor = true;
    }

    bodylz->flags.openCloseColor = bodylz->flags.openCloseColor ? 1 : closecolor;
    if (closecolor) {
        bodylz->flags.closeColorOffset = offsetof(BODYLZ, colors.closeColors);
    }
}

void addGameTextColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* textmain = config->GetValue("colors", "gametext.textmain");
    const char* main = config->GetValue("colors", "gametext.main");
    const char* light = config->GetValue("colors", "gametext.light");
    const char* shadow = config->GetValue("colors", "gametext.shadow");

    bool gametextcolor = false;
    bool hidden = config->GetValue("flags", "gametext.hidden");

    if (textmain != NULL) {
        addRGBColor(&bodylz->colors.gameTextColors1, std::stoul(textmain, nullptr, 16));
        gametextcolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.gameTextColors0[0], std::stoul(main, nullptr, 16));
        gametextcolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.gameTextColors0[1], std::stoul(light, nullptr, 16));
        gametextcolor = true;
    }
    if (shadow != NULL) {
        addRGBAColor(&bodylz->colors.gameTextShadow, std::stoul(shadow, nullptr, 16));
        gametextcolor = true;
    }

    bodylz->flags.gameTextDrawType = hidden ? 2 : gametextcolor;
    if (gametextcolor) {
        bodylz->flags.gameTextOffset = offsetof(BODYLZ, colors.gameTextColors0[0]);
    }
}

void addBottomBGInnerColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* dark = config->GetValue("colors", "bottombginnercolor.dark");
    const char* main = config->GetValue("colors", "bottombginnercolor.main");
    const char* light = config->GetValue("colors", "bottombginnercolor.light");
    const char* shadow = config->GetValue("colors", "bottombginnercolor.shadow");

    bool bottombginnercolor = false;

    if (dark != NULL) {
        addRGBColor(&bodylz->colors.bottomBackgroundInnerColors[0], std::stoul(dark, nullptr, 16));
        bottombginnercolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.bottomBackgroundInnerColors[1], std::stoul(main, nullptr, 16));
        bottombginnercolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.bottomBackgroundInnerColors[2], std::stoul(light, nullptr, 16));
        bottombginnercolor = true;
    }
    if (shadow != NULL) {
        addRGBAColor(&bodylz->colors.bottomBackgroundInnerShadow, std::stoul(shadow, nullptr, 16));
        bottombginnercolor = true;
    }

    bodylz->flags.bottomBackgroundInnerColor = bottombginnercolor;
    if (bottombginnercolor) {
        bodylz->flags.bottomSolidOffset = offsetof(BODYLZ, colors.bottomBackgroundInnerColors[0]);
    }
}

void addBottomBGOuterColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* dark = config->GetValue("colors", "bottombgoutercolor.dark");
    const char* main = config->GetValue("colors", "bottombgoutercolor.main");
    const char* light = config->GetValue("colors", "bottombgoutercolor.light");

    bool bottombgoutercolor = false;

    if (dark != NULL) {
        addRGBColor(&bodylz->colors.bottomBackgroundOuterColors[0], std::stoul(dark, nullptr, 16));
        bottombgoutercolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.bottomBackgroundOuterColors[1], std::stoul(main, nullptr, 16));
        bottombgoutercolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.bottomBackgroundOuterColors[2], std::stoul(light, nullptr, 16));
        bottombgoutercolor = true;
    }

    bodylz->flags.bottomBackgroundOuterColor = bottombgoutercolor;
    if (bottombgoutercolor) {
        bodylz->flags.bottomOuterOffset = offsetof(BODYLZ, colors.bottomBackgroundOuterColors[0]);
    }
}

void addFolderBGColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* dark = config->GetValue("colors", "folderbgcolor.dark");
    const char* main = config->GetValue("colors", "folderbgcolor.main");
    const char* light = config->GetValue("colors", "folderbgcolor.light");
    const char* shadow = config->GetValue("colors", "folderbgcolor.shadow");

    bool folderbgcolor = false;

    if (dark != NULL) {
        addRGBColor(&bodylz->colors.folderBackgroundColors[0], std::stoul(dark, nullptr, 16));
        folderbgcolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.folderBackgroundColors[1], std::stoul(main, nullptr, 16));
        folderbgcolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.folderBackgroundColors[2], std::stoul(light, nullptr, 16));
        folderbgcolor = true;
    }
    if (shadow != NULL) {
        addRGBAColor(&bodylz->colors.folderBackgroundShadow, std::stoul(shadow, nullptr, 16));
        folderbgcolor = true;
    }

    bodylz->flags.folderBackgroundColor = folderbgcolor;
    if (folderbgcolor) {
        bodylz->flags.folderBackgroundOffset = offsetof(BODYLZ, colors.folderBackgroundColors[0]);
    }
}

void addFolderArrowColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* textShadowPos  = config->GetValue("colors", "folderarrowcolor.textshadowpos");
    const char* dark = config->GetValue("colors", "folderarrowcolor.dark");
    const char* main = config->GetValue("colors", "folderarrowcolor.main");
    const char* light = config->GetValue("colors", "folderarrowcolor.light");
    const char* shadow = config->GetValue("colors", "folderarrowcolor.shadow");
    const char* glow = config->GetValue("colors", "folderarrowcolor.glow");
    const char* textshadow = config->GetValue("colors", "folderarrowcolor.textshadow");
    const char* textmain = config->GetValue("colors", "folderarrowcolor.textmain");
    const char* textselected = config->GetValue("colors", "folderarrowcolor.textselected");

    bool folderarrowcolor = false;

    if (textShadowPos != NULL) {
        ufloat pos;
        pos.f = std::stof(textShadowPos) + 0.0f;
        bodylz->colors.folderArrowColors.textShadowPos = pos.u;
    }
    if (dark != NULL) {
        addRGBColor(&bodylz->colors.folderArrowColors.dark, std::stoul(dark, nullptr, 16));
        folderarrowcolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.folderArrowColors.main, std::stoul(main, nullptr, 16));
        folderarrowcolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.folderArrowColors.light, std::stoul(light, nullptr, 16));
        folderarrowcolor = true;
    }
    if (shadow != NULL) {
        addRGBAColor(&bodylz->colors.folderArrowColors.shadow, std::stoul(shadow, nullptr, 16));
        folderarrowcolor = true;
    }
    if (glow != NULL) {
        addRGBColor(&bodylz->colors.folderArrowColors.glow, std::stoul(glow, nullptr, 16));
        folderarrowcolor = true;
    }
    if (textshadow != NULL) {
        addRGBColor(&bodylz->colors.folderArrowColors.textShadow, std::stoul(textshadow, nullptr, 16));
        folderarrowcolor = true;
    }
    if (textmain != NULL) {
        addRGBColor(&bodylz->colors.folderArrowColors.textMain, std::stoul(textmain, nullptr, 16));
        folderarrowcolor = true;
    }
    if (textselected != NULL) {
        addRGBColor(&bodylz->colors.folderArrowColors.textSelected, std::stoul(textselected, nullptr, 16));
        folderarrowcolor = true;
    }

    bodylz->flags.folderArrowColor = folderarrowcolor;
    if (folderarrowcolor) {
        bodylz->flags.folderArrowOffset = offsetof(BODYLZ, colors.folderArrowColors);
    }
}

void addBottomCornerButtonColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* dark = config->GetValue("colors", "bottomcornerbuttoncolor.dark");
    const char* main = config->GetValue("colors", "bottomcornerbuttoncolor.main");
    const char* light = config->GetValue("colors", "bottomcornerbuttoncolor.light");
    const char* shadow = config->GetValue("colors", "bottomcornerbuttoncolor.shadow");
    const char* iconmain = config->GetValue("colors", "bottomcornerbuttoncolor.iconmain");
    const char* iconlight = config->GetValue("colors", "bottomcornerbuttoncolor.iconlight");
    const char* icontextmain = config->GetValue("colors", "bottomcornerbuttoncolor.icontextmain");

    bool bottomcornerbuttoncolor = false;

    if (dark != NULL) {
        addRGBColor(&bodylz->colors.bottomCornerButtonColors[0], std::stoul(dark, nullptr, 16));
        bottomcornerbuttoncolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.bottomCornerButtonColors[1], std::stoul(main, nullptr, 16));
        bottomcornerbuttoncolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.bottomCornerButtonColors[2], std::stoul(light, nullptr, 16));
        bottomcornerbuttoncolor = true;
    }
    if (shadow != NULL) {
        addRGBColor(&bodylz->colors.bottomCornerButtonColors[3], std::stoul(shadow, nullptr, 16));
        bottomcornerbuttoncolor = true;
    }
    if (iconmain != NULL) {
        addRGBColor(&bodylz->colors.bottomCornerButtonColors[4], std::stoul(iconmain, nullptr, 16));
        bottomcornerbuttoncolor = true;
    }
    if (iconlight != NULL) {
        addRGBColor(&bodylz->colors.bottomCornerButtonColors[5], std::stoul(iconlight, nullptr, 16));
        bottomcornerbuttoncolor = true;
    }
    if (icontextmain != NULL) {
        addRGBColor(&bodylz->colors.bottomCornerButtonColors[6], std::stoul(icontextmain, nullptr, 16));
        bottomcornerbuttoncolor = true;
    }

    bodylz->flags.bottomCornerButtonColor = bottomcornerbuttoncolor;
    if (bottomcornerbuttoncolor) {
        bodylz->flags.bottomCornerButtonOffset = offsetof(BODYLZ, colors.bottomCornerButtonColors[0]);
    }
}

void addTopCornerButtonColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* textmain = config->GetValue("colors", "topcornerbuttoncolor.textmain");
    const char* main = config->GetValue("colors", "topcornerbuttoncolor.main");
    const char* light = config->GetValue("colors", "topcornerbuttoncolor.light");
    const char* shadow = config->GetValue("colors", "topcornerbuttoncolor.shadow");

    bool topcornerbuttoncolor = false;

    if (textmain != NULL) {
        addRGBColor(&bodylz->colors.topCornerButtonColors[3], std::stoul(textmain, nullptr, 16));
        topcornerbuttoncolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.topCornerButtonColors[0], std::stoul(main, nullptr, 16));
        topcornerbuttoncolor = true;
    }
    if (light != NULL) {
        addRGBColor(&bodylz->colors.topCornerButtonColors[1], std::stoul(light, nullptr, 16));
        topcornerbuttoncolor = true;
    }
    if (shadow != NULL) {
        addRGBColor(&bodylz->colors.topCornerButtonColors[2], std::stoul(shadow, nullptr, 16));
        topcornerbuttoncolor = true;
    }

    bodylz->flags.topCornerButtonColor = topcornerbuttoncolor;
    if (topcornerbuttoncolor) {
        bodylz->flags.topCornerButtonOffset = offsetof(BODYLZ, colors.topCornerButtonColors[0]);
    }
}

void addDemoTextColor(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* textmain = config->GetValue("colors", "demotextcolor.textmain");
    const char* main = config->GetValue("colors", "demotextcolor.main");

    bool demotextcolor = false;

    if (textmain != NULL) {
        addRGBColor(&bodylz->colors.demoTextColors[1], std::stoul(textmain, nullptr, 16));
        demotextcolor = true;
    }
    if (main != NULL) {
        addRGBColor(&bodylz->colors.demoTextColors[0], std::stoul(main, nullptr, 16));
        demotextcolor = true;
    }

    bodylz->flags.demoTextColor = demotextcolor;
    if (demotextcolor) {
        bodylz->flags.demoTextOffset = offsetof(BODYLZ, colors.demoTextColors[0]);
    }
}

void addTopBGColorParams(BODYLZ* bodylz, CSimpleIniA* config) {
    const char* color = config->GetValue("colors", "topbgcolor");
    const char* gradient = config->GetValue("colors", "topbgcolor.gradient");
    const char* opacity = config->GetValue("colors", "topbgcolor.opacity");
    const char* altopacity = config->GetValue("colors", "topbgcolor.altopacity");
    const char* gradientcolor = config->GetValue("colors", "topbgcolor.gradientcolor");

    bool topbgcolor = false;

    if (color != NULL) {
        addRGBColor(&bodylz->colors.topBackgroundColor, std::stoul(color, nullptr, 16));
        topbgcolor = true;
    }

    if (gradient != NULL) {
        bodylz->colors.topBackgroundColorParams[0] = std::max(std::min((int) round((double) std::stoul(gradient, nullptr, 10) * 255 / 100), 255), 0);
    }
    if (opacity != NULL) {
        bodylz->colors.topBackgroundColorParams[1] = std::max(std::min((int) round((double) std::stoul(opacity, nullptr, 10) * 255 / 100), 255), 0);
    }
    if (altopacity != NULL) {
        bodylz->colors.topBackgroundColorParams[2] = std::max(std::min((int) round((double) std::stoul(altopacity, nullptr, 10) * 255 / 100), 255), 0);
    }
    if (gradientcolor != NULL) {
        bodylz->colors.topBackgroundColorParams[3] = std::max(std::min((int) round((double) std::stoul(gradientcolor, nullptr, 10) * 255 / 100), 255), 0);
    }

    if (topbgcolor) {
        bodylz->flags.top.topBackgroundColorOffset = offsetof(BODYLZ, colors.topBackgroundColor);
    }
}

void addColors(BODYLZ* bodylz, CSimpleIniA* config) {
    addCursorColor(bodylz, config);
    addFolderColor(bodylz, config);
    addFileColor(bodylz, config);
    addArrowButtonColor(bodylz, config);
    addArrowColor(bodylz, config);
    addOpenColor(bodylz, config);
    addCloseColor(bodylz, config);
    addGameTextColor(bodylz, config);
    addBottomBGInnerColor(bodylz, config);
    addBottomBGOuterColor(bodylz, config);
    addFolderBGColor(bodylz, config);
    addFolderArrowColor(bodylz, config);
    addBottomCornerButtonColor(bodylz, config);
    addTopCornerButtonColor(bodylz, config);
    addDemoTextColor(bodylz, config);
}

void extractCursorColor(u8 *data, CSimpleIniA *config) {
    if (data[0x2C] == 1) {
        u32 cursoroffset = u32_from_u8_data(data, 0x30);
        RGBColors cursordark = rgb_from_u8_data(data, cursoroffset);
        RGBColors cursormain = rgb_from_u8_data(data, cursoroffset + 3);
        RGBColors cursorlight = rgb_from_u8_data(data, cursoroffset + 6);
        RGBColors cursorglow = rgb_from_u8_data(data, cursoroffset + 9);

        config->SetValue(COLORS, "cursorcolor.dark", rgb_to_string(cursordark).c_str());
        config->SetValue(COLORS, "cursorcolor.main", rgb_to_string(cursormain).c_str());
        config->SetValue(COLORS, "cursorcolor.light", rgb_to_string(cursorlight).c_str());
        config->SetValue(COLORS, "cursorcolor.glow", rgb_to_string(cursorglow).c_str());
    }
}

void extractFolderColor(u8* data, CSimpleIniA *config) {
    if (data[0x34] == 1) {
        u32 folderoffset = u32_from_u8_data(data, 0x38);
        RGBColors folderdark = rgb_from_u8_data(data, folderoffset);
        RGBColors foldermain = rgb_from_u8_data(data, folderoffset + 3);
        RGBColors folderlight = rgb_from_u8_data(data, folderoffset + 6);
        RGBColors foldershadow = rgb_from_u8_data(data, folderoffset + 9);

        config->SetValue(COLORS, "foldercolor.dark", rgb_to_string(folderdark).c_str());
        config->SetValue(COLORS, "foldercolor.main", rgb_to_string(foldermain).c_str());
        config->SetValue(COLORS, "foldercolor.light", rgb_to_string(folderlight).c_str());
        config->SetValue(COLORS, "foldercolor.shadow", rgb_to_string(foldershadow).c_str());
    }
}

void extractFileColor(u8* data, CSimpleIniA *config) {
    if (data[0x48] == 1) {
        u32 fileoffset = u32_from_u8_data(data, 0x4C);
        RGBColors filedark = rgb_from_u8_data(data, fileoffset);
        RGBColors filemain = rgb_from_u8_data(data, fileoffset + 3);
        RGBColors filelight = rgb_from_u8_data(data, fileoffset + 6);
        RGBAColors fileshadow = rgba_from_u8_data(data, fileoffset + 9);
        config->SetValue(COLORS, "filecolor.dark", rgb_to_string(filedark).c_str());
        config->SetValue(COLORS, "filecolor.main", rgb_to_string(filemain).c_str());
        config->SetValue(COLORS, "filecolor.light", rgb_to_string(filelight).c_str());
        config->SetValue(COLORS, "filecolor.shadow", rgba_to_string(fileshadow).c_str());
    }
}

void extractArrowButtonColor(u8* data, CSimpleIniA* config) {
    if (data[0x5C] == 1) {
        u32 arrowbuttonoffset = u32_from_u8_data(data, 0x60);
        RGBColors arrowbuttondark = rgb_from_u8_data(data, arrowbuttonoffset);
        RGBColors arrowbuttonmain = rgb_from_u8_data(data, arrowbuttonoffset + 3);
        RGBColors arrowbuttonlight = rgb_from_u8_data(data, arrowbuttonoffset + 6);
        RGBAColors arrowbuttonshadow = rgba_from_u8_data(data, arrowbuttonoffset + 9);
        config->SetValue(COLORS, "arrowbuttoncolor.dark", rgb_to_string(arrowbuttondark).c_str());
        config->SetValue(COLORS, "arrowbuttoncolor.main", rgb_to_string(arrowbuttonmain).c_str());
        config->SetValue(COLORS, "arrowbuttoncolor.light", rgb_to_string(arrowbuttonlight).c_str());
        config->SetValue(COLORS, "arrowbuttoncolor.shadow", rgba_to_string(arrowbuttonshadow).c_str());
    }
}

void extractArrowColor(u8* data, CSimpleIniA* config) {
    if (data[0x64] == 1) {
        u32 arrowoffset = u32_from_u8_data(data, 0x68);
        RGBColors arrowborder = rgb_from_u8_data(data, arrowoffset);
        RGBColors arrowunpressed = rgb_from_u8_data(data, arrowoffset + 3);
        RGBColors arrowpressed = rgb_from_u8_data(data, arrowoffset + 6);
        config->SetValue(COLORS, "arrowcolor.border", rgb_to_string(arrowborder).c_str());
        config->SetValue(COLORS, "arrowcolor.unpressed", rgb_to_string(arrowunpressed).c_str());
        config->SetValue(COLORS, "arrowcolor.pressed", rgb_to_string(arrowpressed).c_str());
    }
}

void extractOpenCloseColors(u8* data, CSimpleIniA* config) {
    if (data[0x6C] == 1) {
        u32 openoffset = u32_from_u8_data(data, 0x70);
        u32 closeoffset = u32_from_u8_data(data, 0x74);
        ufloat openshadowpos;
        openshadowpos.u = u32_from_u8_data(data, openoffset);
        RGBColors opendark = rgb_from_u8_data(data, openoffset + 4);
        RGBColors openmain = rgb_from_u8_data(data, openoffset + 7);
        RGBColors openlight = rgb_from_u8_data(data, openoffset + 10);
        RGBAColors openshadow = rgba_from_u8_data(data, openoffset + 13);
        RGBColors openglow = rgb_from_u8_data(data, openoffset + 17);
        RGBColors opentextshadow = rgb_from_u8_data(data, openoffset + 20);
        RGBColors opentextmain = rgb_from_u8_data(data, openoffset + 23);
        RGBColors opentextselected = rgb_from_u8_data(data, openoffset + 26);
        ufloat closeshadowpos;
        closeshadowpos.u = u32_from_u8_data(data, closeoffset);
        RGBColors closedark = rgb_from_u8_data(data, closeoffset + 4);
        RGBColors closemain = rgb_from_u8_data(data, closeoffset + 7);
        RGBColors closelight = rgb_from_u8_data(data, closeoffset + 10);
        RGBAColors closeshadow = rgba_from_u8_data(data, closeoffset + 13);
        RGBColors closeglow = rgb_from_u8_data(data, closeoffset + 17);
        RGBColors closetextshadow = rgb_from_u8_data(data, closeoffset + 20);
        RGBColors closetextmain = rgb_from_u8_data(data, closeoffset + 23);
        RGBColors closetextselected = rgb_from_u8_data(data, closeoffset + 26);

        config->SetValue(COLORS, "open.textshadowpos", std::to_string(openshadowpos.f).c_str());
        config->SetValue(COLORS, "open.dark", rgb_to_string(opendark).c_str());
        config->SetValue(COLORS, "open.main", rgb_to_string(openmain).c_str());
        config->SetValue(COLORS, "open.light", rgb_to_string(openlight).c_str());
        config->SetValue(COLORS, "open.shadow", rgba_to_string(openshadow).c_str());
        config->SetValue(COLORS, "open.glow", rgb_to_string(openglow).c_str());
        config->SetValue(COLORS, "open.textshadow", rgb_to_string(opentextshadow).c_str());
        config->SetValue(COLORS, "open.textmain", rgb_to_string(opentextmain).c_str());
        config->SetValue(COLORS, "open.textselected", rgb_to_string(opentextselected).c_str());
        config->SetValue(COLORS, "close.textshadowpos", std::to_string(closeshadowpos.f).c_str());
        config->SetValue(COLORS, "close.dark", rgb_to_string(closedark).c_str());
        config->SetValue(COLORS, "close.main", rgb_to_string(closemain).c_str());
        config->SetValue(COLORS, "close.light", rgb_to_string(closelight).c_str());
        config->SetValue(COLORS, "close.shadow", rgba_to_string(closeshadow).c_str());
        config->SetValue(COLORS, "close.glow", rgb_to_string(closeglow).c_str());
        config->SetValue(COLORS, "close.textshadow", rgb_to_string(closetextshadow).c_str());
        config->SetValue(COLORS, "close.textmain", rgb_to_string(closetextmain).c_str());
        config->SetValue(COLORS, "close.textselected", rgb_to_string(closetextselected).c_str());
    }
}

void extractGameTextColor(u8* data, CSimpleIniA* config) {
    u32 gametextoffset;
    RGBColors gametextmain;
    RGBColors gametextlight;
    RGBAColors gametextshadow;
    RGBColors gametexttextmain;
    switch (data[0x78]) {
        case 1:
            gametextoffset = u32_from_u8_data(data, 0x7C);
            gametextmain = rgb_from_u8_data(data, gametextoffset);
            gametextlight = rgb_from_u8_data(data, gametextoffset + 3);
            gametextshadow = rgba_from_u8_data(data, gametextoffset + 6);
            gametexttextmain = rgb_from_u8_data(data, gametextoffset + 10);
            config->SetValue(COLORS, "gametext.main", rgb_to_string(gametextmain).c_str());
            config->SetValue(COLORS, "gametext.light", rgb_to_string(gametextlight).c_str());
            config->SetValue(COLORS, "gametext.shadow", rgba_to_string(gametextshadow).c_str());
            config->SetValue(COLORS, "gametext.textmain", rgb_to_string(gametexttextmain).c_str());
            break;
        case 2:
            config->SetValue("flags", "gametext.hidden", "1");
            break;
        default:
            break;
    }
}

void extractFolderBGColor(u8* data, CSimpleIniA* config) {
    if (data[0x90] == 1) {
        u32 folderbgoffset = u32_from_u8_data(data, 0x94);
        RGBColors folderbgdark = rgb_from_u8_data(data, folderbgoffset);
        RGBColors folderbgmain = rgb_from_u8_data(data, folderbgoffset + 3);
        RGBColors folderbglight = rgb_from_u8_data(data, folderbgoffset + 6);
        RGBAColors folderbgshadow = rgba_from_u8_data(data, folderbgoffset + 9);
        config->SetValue(COLORS, "folderbgcolor.dark", rgb_to_string(folderbgdark).c_str());
        config->SetValue(COLORS, "folderbgcolor.main", rgb_to_string(folderbgmain).c_str());
        config->SetValue(COLORS, "folderbgcolor.light", rgb_to_string(folderbglight).c_str());
        config->SetValue(COLORS, "folderbgcolor.shadow", rgba_to_string(folderbgshadow).c_str());
    }
}

void extractFolderArrowColor(u8* data, CSimpleIniA* config) {
    if (data[0x98] == 1) {
        u32 folderarrowoffset = u32_from_u8_data(data, 0x9C);
        ufloat folderarrowshadowpos;
        folderarrowshadowpos.u = u32_from_u8_data(data, folderarrowoffset);
        RGBColors folderarrowdark = rgb_from_u8_data(data, folderarrowoffset + 4);
        RGBColors folderarrowmain = rgb_from_u8_data(data, folderarrowoffset + 7);
        RGBColors folderarrowlight = rgb_from_u8_data(data, folderarrowoffset + 10);
        RGBAColors folderarrowshadow = rgba_from_u8_data(data, folderarrowoffset + 13);
        RGBColors folderarrowglow = rgb_from_u8_data(data, folderarrowoffset + 17);
        RGBColors folderarrowtextshadow = rgb_from_u8_data(data, folderarrowoffset + 20);
        RGBColors folderarrowtextmain = rgb_from_u8_data(data, folderarrowoffset + 23);
        RGBColors folderarrowtextselected = rgb_from_u8_data(data, folderarrowoffset + 26);

        config->SetValue(COLORS, "folderarrowcolor.textshadowpos", std::to_string(folderarrowshadowpos.f).c_str());
        config->SetValue(COLORS, "folderarrowcolor.dark", rgb_to_string(folderarrowdark).c_str());
        config->SetValue(COLORS, "folderarrowcolor.main", rgb_to_string(folderarrowmain).c_str());
        config->SetValue(COLORS, "folderarrowcolor.light", rgb_to_string(folderarrowlight).c_str());
        config->SetValue(COLORS, "folderarrowcolor.shadow", rgba_to_string(folderarrowshadow).c_str());
        config->SetValue(COLORS, "folderarrowcolor.glow", rgb_to_string(folderarrowglow).c_str());
        config->SetValue(COLORS, "folderarrowcolor.textshadow", rgb_to_string(folderarrowtextshadow).c_str());
        config->SetValue(COLORS, "folderarrowcolor.textmain", rgb_to_string(folderarrowtextmain).c_str());
        config->SetValue(COLORS, "folderarrowcolor.textselected", rgb_to_string(folderarrowtextselected).c_str());
    }
}

void extractBottomCornerButtonColor(u8* data, CSimpleIniA* config) {
    if (data[0xA0] == 1) {
        u32 bottomcorneroffset = u32_from_u8_data(data, 0xA4);
        RGBColors bottomcornerdark = rgb_from_u8_data(data, bottomcorneroffset);
        RGBColors bottomcornermain = rgb_from_u8_data(data, bottomcorneroffset + 3);
        RGBColors bottomcornerlight = rgb_from_u8_data(data, bottomcorneroffset + 6);
        RGBColors bottomcornershadow = rgb_from_u8_data(data, bottomcorneroffset + 9);
        RGBColors bottomcornericonmain = rgb_from_u8_data(data, bottomcorneroffset + 12);
        RGBColors bottomcornericonlight = rgb_from_u8_data(data, bottomcorneroffset + 15);
        RGBColors bottomcornericontextmain = rgb_from_u8_data(data, bottomcorneroffset + 18);
        config->SetValue(COLORS, "bottomcornerbuttoncolor.dark", rgb_to_string(bottomcornerdark).c_str());
        config->SetValue(COLORS, "bottomcornerbuttoncolor.main", rgb_to_string(bottomcornermain).c_str());
        config->SetValue(COLORS, "bottomcornerbuttoncolor.light", rgb_to_string(bottomcornerlight).c_str());
        config->SetValue(COLORS, "bottomcornerbuttoncolor.shadow", rgb_to_string(bottomcornershadow).c_str());
        config->SetValue(COLORS, "bottomcornerbuttoncolor.iconmain", rgb_to_string(bottomcornericonmain).c_str());
        config->SetValue(COLORS, "bottomcornerbuttoncolor.iconlight", rgb_to_string(bottomcornericonlight).c_str());
        config->SetValue(COLORS, "bottomcornerbuttoncolor.icontextmain", rgb_to_string(bottomcornericontextmain).c_str());
    }
}

void extractTopCornerButtonColor(u8* data, CSimpleIniA* config) {
    if (data[0xA8] == 1) {
        u32 topcorneroffset = u32_from_u8_data(data, 0xAC);
        RGBColors topcornermain = rgb_from_u8_data(data, topcorneroffset);
        RGBColors topcornerlight = rgb_from_u8_data(data, topcorneroffset + 3);
        RGBColors topcornershadow = rgb_from_u8_data(data, topcorneroffset + 6);
        RGBColors topcornertextmain = rgb_from_u8_data(data, topcorneroffset + 9);
        config->SetValue(COLORS, "topcornerbuttoncolor.main", rgb_to_string(topcornermain).c_str());
        config->SetValue(COLORS, "topcornerbuttoncolor.light", rgb_to_string(topcornerlight).c_str());
        config->SetValue(COLORS, "topcornerbuttoncolor.shadow", rgb_to_string(topcornershadow).c_str());
        config->SetValue(COLORS, "topcornerbuttoncolor.textmain", rgb_to_string(topcornertextmain).c_str());
    }
}

void extractDemoTextColor(u8* data, CSimpleIniA* config) {
    if (data[0xB0] == 1) {
        u32 demotextoffset = u32_from_u8_data(data, 0xB4);
        RGBColors demotextmain = rgb_from_u8_data(data, demotextoffset);
        RGBColors demotexttextmain = rgb_from_u8_data(data, demotextoffset + 3);
        config->SetValue(COLORS, "demotextcolor.main", rgb_to_string(demotextmain).c_str());
        config->SetValue(COLORS, "demotextcolor.textmain", rgb_to_string(demotexttextmain).c_str());
    }
}
