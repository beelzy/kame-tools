#ifndef BODYLZ_H
#define BODYLZ_H

#include "../types.h"
#include "../pc/SimpleIni.h"

#define BODYLZ_WIDTH_WIDE 1024
#define BODYLZ_WIDTH 512
#define BODYLZ_HEIGHT 256

#define BODYLZ_FOLDER_WIDTH 128
#define BODYLZ_FOLDER_HEIGHT 64

#define BODYLZ_LARGE_FILE_WIDTH 64
#define BODYLZ_LARGE_FILE_HEIGHT 128

#define BODYLZ_SMALL_FILE_WIDTH 32
#define BODYLZ_SMALL_FILE_HEIGHT 64

#define BODYLZ_SOLID_COLOR_SIZE 64

static const char* COLORS = "colors";

typedef enum {
    BODYLZ_NORMAL = 0,
    BODYLZ_WIDE = 1,
    BODYLZ_TOP_WIDE = 2,
    BODYLZ_BOTTOM_WIDE = 3
} BODYLZType;

typedef enum {
    BODYLZ_TOP_NONE = 0,
    BODYLZ_TOP_SOLID_COLOR = 1,
    BODYLZ_TOP_SOLID_COLOR_TEXTURE = 2,
    BODYLZ_TOP_TEXTURE = 3
} BODYLZTopDrawType;

typedef enum {
    BODYLZ_TOP_FRAME_FASTSCROLL = 0,
    BODYLZ_TOP_FRAME_SINGLE = 1,
    BODYLZ_TOP_FRAME_INVALID = 2,
    BODYLZ_TOP_FRAME_SLOWSCROLL = 3
} BODYLZTopFrameType;

typedef enum {
    BODYLZ_BOTTOM_NONE = 0,
    BODYLZ_BOTTOM_SOLID_COLOR = 1,
    BODYLZ_BOTTOM_INVALID = 2,
    BODYLZ_BOTTOM_TEXTURE = 3
} BODYLZBottomDrawType;

typedef enum {
    BODYLZ_BOTTOM_FRAME_FASTSCROLL = 0,
    BODYLZ_BOTTOM_FRAME_SINGLE = 1,
    BODYLZ_BOTTOM_FRAME_PAGESCROLL = 2,
    BODYLZ_BOTTOM_FRAME_SLOWSCROLL = 3,
    BODYLZ_BOTTOM_FRAME_BOUNCESCROLL = 4
} BODYLZBottomFrameType;

typedef enum {
    BODYLZ_GAMETEXT_DEFAULT = 0,
    BODYLZ_GAMETEXT_COLORED = 1,
    BODYLZ_GAMETEXT_HIDDEN = 2
} BODYLZGameTextDrawType;

typedef struct {
    u8 R;
    u8 G;
    u8 B;
} RGBColors;

typedef struct {
    u8 R;
    u8 G;
    u8 B;
    u8 A;
} RGBAColors;

typedef struct {
    u32 topDrawType;
    u32 topFrameType;
    u32 topBackgroundColorOffset;
    u32 topTextureOffset;
    u32 topTextureExtOffset;
} BODYLZTopFlags;

typedef struct {
    u32 bottomDrawType;
    u32 bottomFrameType;
    u32 bottomTextureOffset;
} BODYLZBottomFlags;

typedef struct {
    BODYLZTopFlags top;
    BODYLZBottomFlags bottom;
    u32 cursorColor;
    u32 cursorOffset;
    u32 folderColor;
    u32 folderOffset;
    u32 folderTexture;
    u32 folderClosedOffset;
    u32 folderOpenedOffset;
    u32 fileColor;
    u32 fileOffset;
    u32 fileTexture;
    u32 fileLargeOffset;
    u32 fileSmallOffset;
    u32 arrowButtonColor;
    u32 arrowButtonOffset;
    u32 arrowColor;
    u32 arrowOffset;
    u32 openCloseColor;
    u32 openColorOffset;
    u32 closeColorOffset;
    u32 gameTextDrawType;
    u32 gameTextOffset;
    u32 bottomBackgroundInnerColor;
    u32 bottomSolidOffset;
    u32 bottomBackgroundOuterColor;
    u32 bottomOuterOffset;
    u32 folderBackgroundColor;
    u32 folderBackgroundOffset;
    u32 folderArrowColor;
    u32 folderArrowOffset;
    u32 bottomCornerButtonColor;
    u32 bottomCornerButtonOffset;
    u32 topCornerButtonColor;
    u32 topCornerButtonOffset;
    u32 demoTextColor;
    u32 demoTextOffset;
    u32 sfx;
    u32 cwavLength;
    u32 cwavOffset;
} BODYLZFlags;

typedef struct {
    u32 textShadowPos;
    RGBColors dark;
    RGBColors main;
    RGBColors light;
    RGBAColors shadow;
    RGBColors glow;
    RGBColors textShadow;
    RGBColors textMain;
    RGBColors textSelected;
    RGBColors unused;
} BODYLZColorSet;

typedef struct {
    RGBColors topBackgroundColor;
    u8 topBackgroundColorParams[4];
    RGBColors cursorColors[4];
    RGBColors folderColors[4];
    RGBColors fileColors[3];
    RGBAColors fileShadow;
    RGBColors arrowButtonColors[3];
    RGBAColors arrowButtonShadow;
    RGBColors arrowColors[3];
    BODYLZColorSet openColors;
    BODYLZColorSet closeColors;
    RGBColors gameTextColors0[2];
    RGBAColors gameTextShadow;
    RGBColors gameTextColors1;
    RGBColors bottomBackgroundInnerColors[3];
    RGBAColors bottomBackgroundInnerShadow;
    RGBColors bottomBackgroundOuterColors[3];
    RGBColors folderBackgroundColors[3];
    RGBAColors folderBackgroundShadow;
    BODYLZColorSet folderArrowColors;
    RGBColors bottomCornerButtonColors[7];
    RGBColors topCornerButtonColors[4];
    RGBColors demoTextColors[2];
} BODYLZColors;

typedef struct {
    u16 topTexture[BODYLZ_WIDTH* BODYLZ_HEIGHT];
    u8 topExt[BODYLZ_SOLID_COLOR_SIZE * BODYLZ_SOLID_COLOR_SIZE];
    u16 bottomTexture[BODYLZ_WIDTH * BODYLZ_HEIGHT];
    RGBColors folderClose[BODYLZ_FOLDER_WIDTH * BODYLZ_FOLDER_HEIGHT];
    RGBColors folderOpen[BODYLZ_FOLDER_WIDTH * BODYLZ_FOLDER_HEIGHT];
    RGBColors largeFile[BODYLZ_LARGE_FILE_WIDTH * BODYLZ_LARGE_FILE_HEIGHT];
    RGBColors smallFile[BODYLZ_SMALL_FILE_WIDTH * BODYLZ_SMALL_FILE_HEIGHT];
} BODYLZTextures;

typedef struct {
    u16 topTexture[BODYLZ_WIDTH_WIDE * BODYLZ_HEIGHT];
    u8 topExt[BODYLZ_SOLID_COLOR_SIZE * BODYLZ_SOLID_COLOR_SIZE];
    u16 bottomTexture[BODYLZ_WIDTH_WIDE * BODYLZ_HEIGHT];
    RGBColors folderClose[BODYLZ_FOLDER_WIDTH * BODYLZ_FOLDER_HEIGHT];
    RGBColors folderOpen[BODYLZ_FOLDER_WIDTH * BODYLZ_FOLDER_HEIGHT];
    RGBColors largeFile[BODYLZ_LARGE_FILE_WIDTH * BODYLZ_LARGE_FILE_HEIGHT];
    RGBColors smallFile[BODYLZ_SMALL_FILE_WIDTH * BODYLZ_SMALL_FILE_HEIGHT];
} BODYLZTexturesWide;

typedef struct {
    u16 topTexture[BODYLZ_WIDTH_WIDE * BODYLZ_HEIGHT];
    u8 topExt[BODYLZ_SOLID_COLOR_SIZE * BODYLZ_SOLID_COLOR_SIZE];
    u16 bottomTexture[BODYLZ_WIDTH * BODYLZ_HEIGHT];
    RGBColors folderClose[BODYLZ_FOLDER_WIDTH * BODYLZ_FOLDER_HEIGHT];
    RGBColors folderOpen[BODYLZ_FOLDER_WIDTH * BODYLZ_FOLDER_HEIGHT];
    RGBColors largeFile[BODYLZ_LARGE_FILE_WIDTH * BODYLZ_LARGE_FILE_HEIGHT];
    RGBColors smallFile[BODYLZ_SMALL_FILE_WIDTH * BODYLZ_SMALL_FILE_HEIGHT];
} BODYLZTexturesTopWide;

typedef struct {
    u16 topTexture[BODYLZ_WIDTH * BODYLZ_HEIGHT];
    u8 topExt[BODYLZ_SOLID_COLOR_SIZE * BODYLZ_SOLID_COLOR_SIZE];
    u16 bottomTexture[BODYLZ_WIDTH_WIDE * BODYLZ_HEIGHT];
    RGBColors folderClose[BODYLZ_FOLDER_WIDTH * BODYLZ_FOLDER_HEIGHT];
    RGBColors folderOpen[BODYLZ_FOLDER_WIDTH * BODYLZ_FOLDER_HEIGHT];
    RGBColors largeFile[BODYLZ_LARGE_FILE_WIDTH * BODYLZ_LARGE_FILE_HEIGHT];
    RGBColors smallFile[BODYLZ_SMALL_FILE_WIDTH * BODYLZ_SMALL_FILE_HEIGHT];
} BODYLZTexturesBottomWide;

typedef struct {
    u32 size;
    bool enabled;
    void* data;
} BODYLZCWav;

typedef struct {
    BODYLZCWav cwavs[8];
} BODYLZSfx;

typedef struct {
    u8 version;
    char unused;
    u32 bgmEnabled;
    u8 unused1[3];
    BODYLZFlags flags;
    u8 unused2;
    BODYLZColors colors;
} BODYLZ;

void addCursorColor(BODYLZ* bodylz, CSimpleIniA* config);
void addFolderColor(BODYLZ* bodylz, CSimpleIniA* config);
void addFileColor(BODYLZ* bodylz, CSimpleIniA* config);
void addArrowButtonColor(BODYLZ* bodylz, CSimpleIniA* config);
void addArrowColor(BODYLZ* bodylz, CSimpleIniA* config);
void addOpenColor(BODYLZ* bodylz, CSimpleIniA* config);
void addCloseColor(BODYLZ* bodylz, CSimpleIniA* config);
void addGameTextColor(BODYLZ* bodylz, CSimpleIniA* config);
void addBottomBGInnerColor(BODYLZ* bodylz, CSimpleIniA* config);
void addBottomBGOuterColor(BODYLZ* bodylz, CSimpleIniA* config);
void addFolderBGColor(BODYLZ* bodylz, CSimpleIniA* config);
void addFolderArrowColor(BODYLZ* bodylz, CSimpleIniA* config);
void addBottomCornerButtonColor(BODYLZ* bodylz, CSimpleIniA* config);
void addTopCornerButtonColor(BODYLZ* bodylz, CSimpleIniA* config);
void addDemoTextColor(BODYLZ* bodylz, CSimpleIniA* config);

void addTopBGColorParams(BODYLZ* bodylz, CSimpleIniA* config);

void addColors(BODYLZ* bodylz, CSimpleIniA* config);

void extractCursorColor(u8* data, CSimpleIniA* config);
void extractFolderColor(u8* data, CSimpleIniA* config);
void extractFileColor(u8* data, CSimpleIniA* config);
void extractArrowButtonColor(u8* data, CSimpleIniA* config);
void extractArrowColor(u8* data, CSimpleIniA* config);
void extractOpenCloseColors(u8* data, CSimpleIniA* config);
void extractGameTextColor(u8* data, CSimpleIniA* config);
void extractFolderBGColor(u8* data, CSimpleIniA* config);
void extractFolderArrowColor(u8* data, CSimpleIniA* config);
void extractBottomCornerButtonColor(u8* data, CSimpleIniA* config);
void extractTopCornerButtonColor(u8* data, CSimpleIniA* config);
void extractDemoTextColor(u8* data, CSimpleIniA* config);

RGBColors rgb_from_u8_data(u8* data, u32 index);
RGBAColors rgba_from_u8_data(u8* data, u32 index);
std::string rgb_to_string(RGBColors color);
std::string rgba_to_string(RGBAColors color);

#endif
