#ifndef LZ11_H
#define LZ11_H

#include "../types.h"

void* lz11_compress(u32* size, void* input, u32 inputSize);
void lz11_decompress(const u8 *src, u8 *dst, int size);
u32 lz11_decompressed_size(const u8 *src);

#endif
