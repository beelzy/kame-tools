#include "cmd.h"

#include "3ds/bodylz.h"
#include "3ds/cbmd.h"
#include "3ds/cwav.h"
#include "3ds/data.h"
#include "3ds/lz11.h"
#include "3ds/smdh.h"
#include "pc/color.h"
#include "pc/path.hpp"
#include "pc/SimpleIni.h"
#include "pc/stb_image.h"
#include "pc/stbi_image_write.h"
#include "pc/stb_vorbis.h"
#include "pc/wav.h"
#include "pc/miniz.h"

#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <codecvt>
#include <locale>

#include <algorithm>
#include <map>
#include <vector>

u32 extData[11] = {0x0000, 0x6400, 0x0000, 0x6400, 0x6400, 0x0000, 0x6400, 0x6400, 0x0000, 0x0000, 0x0000};
u32 cwavHeader[2] = {0x0002, 0x0064};
u32 cwavEnabled = 0x0050;
u32 cwavDisabled = 0x0001;
u32 cwavNoSize = 0x0000;

static const char* INFO_GROUP = "info";
static const char* FRAMES = "frames";
static const char* TEXTURES = "textures";
static const char* AUDIO = "audio";

static void utf8_to_utf16(u16* dst, const std::string& src, size_t maxLen) {

    if(maxLen == 0) {
        return;
    }

    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> converter;
    std::u16string str16 = converter.from_bytes(src.data());

    size_t copyLen = str16.length() * sizeof(char16_t);
    if (copyLen > maxLen) {
        copyLen = maxLen;
    }

    memcpy(dst, str16.data(), copyLen);
}

static std::string u16_to_string(char16_t *data) {
    std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t > converter;
    return converter.to_bytes(data);
}

static void* read_file(u32* size, const std::string& file) {
    FILE* fd = fopen(file.c_str(), "rb");
    if(fd == NULL) {
        perror("ERROR: Could not open file for reading");
        return NULL;
    }

    long tell = 0;
    if(fseek(fd, 0, SEEK_END) != 0 || (tell = ftell(fd)) < 0 || fseek(fd, 0, SEEK_SET) != 0) {
        fclose(fd);

        perror("ERROR: Failed to determine file size");
        return NULL;
    }

    size_t bufferSize = (size_t) tell;
    void* buffer = malloc(bufferSize);
    if(buffer == NULL) {
        fclose(fd);

        printf("ERROR: Could not allocate memory for file data.\n");
        return NULL;
    }

    size_t readRet = fread(buffer, 1, bufferSize, fd);

    fclose(fd);

    if(readRet != bufferSize) {
        free(buffer);

        perror("ERROR: Failed to read file");
        return NULL;
    }

    if(size != NULL) {
        *size = bufferSize;
    }

    return buffer;
}

static bool write_file(void* contents, u32 size, const std::string& file) {
    FILE* fd = fopen(file.c_str(), "wb");
    if(fd == NULL) {
        perror("ERROR: Could not open file for writing");
        return false;
    }

    size_t writeRet = fwrite(contents, 1, size, fd);

    fclose(fd);

    if(writeRet != size) {
        perror("ERROR: Failed to write file");
        return false;
    }

    return true;
}

static void* load_image(const std::string& file, u32 width, u32 height) {
    int imgWidth = 0;
    int imgHeight = 0;
    int imgDepth = 0;
    unsigned char* img = stbi_load(file.c_str(), &imgWidth, &imgHeight, &imgDepth, STBI_rgb_alpha);
    if(img == NULL) {
        printf("ERROR: Could not load image file: %s.\n", stbi_failure_reason());
        return NULL;
    }

    if(width == 0) {
        width = (u32) imgWidth;
    }

    if(height == 0) {
        height = (u32) imgHeight;
    }

    if((u32) imgWidth != width || (u32) imgHeight != height) {
        stbi_image_free(img);

        printf("ERROR: Image must be exactly %d x %d in size.\n", width, height);
        return NULL;
    }

    return img;
}

static void free_image(void* img) {
    stbi_image_free(img);
}

static void tiles_to_data(void* out, void* img, u32 width, u32 height, PixelFormat format) {
    for(u32 y = 0; y < height; y++) {
        for(u32 x = 0; x < width; x++) {
            u32 index = (((y >> 3) * (width >> 3) + (x >> 3)) << 6) + ((x & 1) | ((y & 1) << 1) | ((x & 2) << 1) | ((y & 2) << 2) | ((x & 4) << 2) | ((y & 4) << 3));

            RGBColors truecolor;
            if (format == RGB565) {
                u16 pixel = ((u16*) img)[index];
                truecolor.R = (u8) (pixel >> 8);
                truecolor.B = (u8) (pixel << 3);
                truecolor.G = (u8) (pixel >> 3);
            } else if (format == A8) {
                u8 color = ((u8*) img)[index];
                truecolor.R = color;
                truecolor.G = color;
                truecolor.B = color;
            } else if (format == RGB888) {
                u8* colorset = &((u8*) img)[index * sizeof(RGBColors)];
                truecolor.R = colorset[2];
                truecolor.G = colorset[1];
                truecolor.B = colorset[0];
                //printf("pixel %d, %#08x, %#08x, %#08x\n", y * width + x, colorset[2], colorset[1], colorset[0]);
            }
            ((RGBColors*) out)[y * width + x] = truecolor;
        }
    }
}

static void image_data_to_tiles(void* out, void* img, u32 width, u32 height, PixelFormat format, bool dither=true) {
    for(u32 y = 0; y < height; y++) {
        for(u32 x = 0; x < width; x++) {
            u32 index = (((y >> 3) * (width >> 3) + (x >> 3)) << 6) + ((x & 1) | ((y & 1) << 1) | ((x & 2) << 1) | ((y & 2) << 2) | ((x & 4) << 2) | ((y & 4) << 3));

            u8* pixel = &((u8*) img)[(y * width + x) * sizeof(u32)];
            u16 color = 0;
            if(format == RGB565) {
                    float alpha = pixel[3] / 255.0f;
                    color = dither ? dither_color_xy(x, y, pixel) : (u16) ((((u8) (pixel[0] * alpha) & ~0x7) << 8) | (((u8) (pixel[1] * alpha) & ~0x3) << 3) | ((u8) (pixel[2] * alpha) >> 3));
            } else if(format == RGBA4444) {
                color = (u16) (((pixel[0] & ~0xF) << 8) | ((pixel[1] & ~0xF) << 4)| (pixel[2] & ~0xF) | (pixel[3] >> 4));
            }
            if(format != RGB888 && format != A8) {
                ((u16*) out)[index] = color;
            }

            RGBColors truecolor;
            if(format == RGB888) {
                truecolor.R = (u8) (pixel[2]);
                truecolor.G = (u8) (pixel[1]);
                truecolor.B = (u8) (pixel[0]);
                ((RGBColors*) out)[index] = truecolor;
            } else if (format == A8) {
                double R_linear = sRGB_to_linear((double) pixel[2] / 255.0);
                double G_linear = sRGB_to_linear((double) pixel[1] / 255.0);
                double B_linear = sRGB_to_linear((double) pixel[0] / 255.0);
                double gray_linear = 0.2126 * R_linear + 0.7152 * G_linear + 0.0722 * B_linear;

                ((u8*) out)[index] = (u8) round(linear_to_sRGB(gray_linear) * 255);
            }
        }
    }
}

static void* convert_to_cgfx(u32* size, const std::string& file) {
    u32 bufferSize = BANNER_CGFX_HEADER_LENGTH + (256 * 128 * sizeof(u16));
    void* buffer = malloc(bufferSize);
    if(buffer == NULL) {
        printf("ERROR: Could not allocate memory for CGFX data.\n");
        return NULL;
    }

    void* img = load_image(file, 256, 128);
    if(img == NULL) {
        free(buffer);

        return NULL;
    }

    memcpy(buffer, BANNER_CGFX_HEADER, BANNER_CGFX_HEADER_LENGTH);
    image_data_to_tiles((u16*) ((u8*) buffer + BANNER_CGFX_HEADER_LENGTH), img, 256, 128, RGBA4444);

    free_image(img);

    if(size != NULL) {
        *size = bufferSize;
    }

    return buffer;
}

static void* convert_to_cwav(u32* size, const std::string& file, bool loop, u32 loopStartFrame, u32 loopEndFrame) {
    FILE* fd = fopen(file.c_str(), "rb");
    if(fd == NULL) {
        perror("ERROR: Failed to open file for CWAV conversion");
        return NULL;
    }

    char magic[4];
    if(fread(magic, 1, sizeof(magic), fd) != sizeof(magic)) {
        fclose(fd);

        perror("ERROR: Failed to read audio file magic");
        return NULL;
    }

    rewind(fd);

    CWAV cwav;
    memset(&cwav, 0, sizeof(cwav));

    cwav.loop = loop;
    cwav.loopStartFrame = loopStartFrame;
    cwav.loopEndFrame = loopEndFrame;

    if(memcmp(magic, "RIFF", sizeof(magic)) == 0) {
        WAV* wav = wav_read(fd);
        if(wav != NULL) {
            cwav.channels = wav->format.numChannels;
            cwav.sampleRate = wav->format.sampleRate;
            cwav.bitsPerSample = wav->format.bitsPerSample;

            cwav.dataSize = wav->data.size;
            cwav.data = calloc(wav->data.size, sizeof(u8));
            if(cwav.data != NULL) {
                memcpy(cwav.data, wav->data.data, wav->data.size);
            } else {
                printf("ERROR: Could not allocate memory for CWAV sample data.\n");
            }

            wav_free(wav);
        }
    } else if(memcmp(magic, "OggS", sizeof(magic)) == 0) {
        int error = 0;
        stb_vorbis* vorbis = stb_vorbis_open_file(fd, false, &error, NULL);

        if(vorbis != NULL) {
            stb_vorbis_info info = stb_vorbis_get_info(vorbis);
            u32 sampleCount = stb_vorbis_stream_length_in_samples(vorbis) * info.channels;

            cwav.channels = (u32) info.channels;
            cwav.sampleRate = info.sample_rate;
            cwav.bitsPerSample = sizeof(u16) * 8;

            cwav.dataSize = sampleCount * sizeof(u16);
            cwav.data = calloc(sampleCount, sizeof(u16));
            if(cwav.data != NULL) {
                stb_vorbis_get_samples_short_interleaved(vorbis, info.channels, (short*) cwav.data, sampleCount);
            } else {
                printf("ERROR: Could not allocate memory for CWAV sample data.\n");
            }

            stb_vorbis_close(vorbis);
        } else {
            printf("ERROR: Failed to open vorbis file: %d.\n", error);
        }
    } else {
        printf("ERROR: Audio file magic '%c%c%c%c' unrecognized.\n", magic[0], magic[1], magic[2], magic[3]);
    }

    fclose(fd);

    if(cwav.data == NULL) {
        return NULL;
    }

    void* ret = cwav_build(size, cwav);

    free(cwav.data);
    return ret;
}

static int cmd_make_banner(const std::string* images, const std::string& audio, const std::string* cgfxFiles, const std::string& cwavFile, const std::string& output) {
    CBMD cbmd;
    memset(&cbmd, 0, sizeof(cbmd));

    bool error = false;

    for(u32 i = 0; i < CBMD_NUM_CGFXS && !error; i++) {
        if(!cgfxFiles[i].empty()) {
            error = (cbmd.cgfxs[i] = read_file(&cbmd.cgfxSizes[i], cgfxFiles[i])) == NULL;
        } else if(!images[i].empty()) {
            error = (cbmd.cgfxs[i] = convert_to_cgfx(&cbmd.cgfxSizes[i], images[i])) == NULL;
        }
    }

    if(!error) {
        if(!cwavFile.empty()) {
            error = (cbmd.cwav = read_file(&cbmd.cwavSize, cwavFile)) == NULL;
        } else {
            error = (cbmd.cwav = convert_to_cwav(&cbmd.cwavSize, audio, false, 0, 0)) == NULL;
        }
    }

    u32 bnrSize = 0;
    void* bnr = !error ? bnr_build(&bnrSize, cbmd) : NULL;

    for(u32 i = 0; i < CBMD_NUM_CGFXS && !error; i++) {
        if(cbmd.cgfxs[i] != NULL) {
            free(cbmd.cgfxs[i]);
        }
    }

    if(cbmd.cwav != NULL) {
        free(cbmd.cwav);
    }

    if(bnr == NULL || !write_file(bnr, bnrSize, output)) {
        return 1;
    }

    printf("Created banner \"%s\".\n", output.c_str());
    return 0;
}

static int cmd_make_smdh(SMDH& smdh, const std::string& icon, const std::string& smallicon, const std::string& output, bool dither = true) {
    u8* icon48Data = (u8*) load_image(icon.c_str(), SMDH_LARGE_ICON_SIZE, SMDH_LARGE_ICON_SIZE);
    if(icon48Data == NULL) {
        return 1;
    }

    void (*icon24free)(void*);
    u8* icon24Data;

    if (smallicon.compare(icon) != 0) {
        icon24Data = (u8*) load_image(smallicon.c_str(), SMDH_SMALL_ICON_SIZE, SMDH_SMALL_ICON_SIZE);
        icon24free = &free_image;
    }
    if (smallicon.compare(icon) == 0 || icon24Data == NULL) {
        icon24Data = (u8*) malloc(SMDH_SMALL_ICON_SIZE * SMDH_SMALL_ICON_SIZE * sizeof(u32));
        icon24free = &free;
        if (!icon24Data) {
            free_image(icon48Data);
            return 1;
        }
        u32 scale = SMDH_LARGE_ICON_SIZE / SMDH_SMALL_ICON_SIZE;
        u32 samples = scale * scale;
        for(u32 y = 0; y < SMDH_LARGE_ICON_SIZE; y += scale) {
            for(u32 x = 0; x < SMDH_LARGE_ICON_SIZE; x += scale) {
                u32 r = 0;
                u32 g = 0;
                u32 b = 0;
                u32 a = 0;

                for(u32 oy = 0; oy < scale; oy++) {
                    for(u32 ox = 0; ox < scale; ox++) {
                        int i = ((y + oy) * SMDH_LARGE_ICON_SIZE + (x + ox)) * sizeof(u32);
                        r += icon48Data[i + 0];
                        g += icon48Data[i + 1];
                        b += icon48Data[i + 2];
                        a += icon48Data[i + 3];
                    }
                }

                int i = ((y / scale) * SMDH_SMALL_ICON_SIZE + (x / scale)) * sizeof(u32);
                icon24Data[i + 0] = (u8) (r / samples);
                icon24Data[i + 1] = (u8) (g / samples);
                icon24Data[i + 2] = (u8) (b / samples);
                icon24Data[i + 3] = (u8) (a / samples);
            }
        }
    }

    image_data_to_tiles(smdh.largeIcon, icon48Data, SMDH_LARGE_ICON_SIZE, SMDH_LARGE_ICON_SIZE, RGB565, dither);
    image_data_to_tiles(smdh.smallIcon, icon24Data, SMDH_SMALL_ICON_SIZE, SMDH_SMALL_ICON_SIZE, RGB565, dither);

    icon24free(icon24Data);
    free_image(icon48Data);

    if(!write_file(&smdh, sizeof(SMDH), output)) {
        return 1;
    }

    printf("Created SMDH \"%s\".\n", output.c_str());
    return 0;
}

static int cmd_make_cwav(const std::string& input, const std::string& output, bool loop, u32 loopStartFrame, u32 loopEndFrame) {
    u32 cwavSize = 0;
    void* cwav = convert_to_cwav(&cwavSize, input, loop, loopStartFrame, loopEndFrame);
    if(cwav == NULL || !write_file(cwav, cwavSize, output)) {
        return 1;
    }

    printf("Created CWAV \"%s\".\n", output.c_str());
    return 0;
}

static void loadFolderTexture(void* folderOpenBuffer, void* folderCloseBuffer, BODYLZ* bodylz, CSimpleIniA* config, u32 openOffset, u32 closeOffset, bool dither) {
    bool folderEnabled = false;
    const char* folderOpenPath = config->GetValue("textures", "folderopen.image");
    const char* folderClosedPath = config->GetValue("textures", "folderclose.image");
    if (folderOpenPath != NULL) {
        u8* folderOpened = (u8*) load_image(folderOpenPath, BODYLZ_FOLDER_WIDTH, BODYLZ_FOLDER_HEIGHT);
        if (folderOpened == NULL) {
            return;
        }
        image_data_to_tiles(folderOpenBuffer, folderOpened, BODYLZ_FOLDER_WIDTH, BODYLZ_FOLDER_HEIGHT, RGB888, dither);
        free_image(folderOpened);

        folderEnabled = true;
    }

    if (folderClosedPath != NULL) {
        u8* folderClosed = (u8*) load_image(folderClosedPath, BODYLZ_FOLDER_WIDTH, BODYLZ_FOLDER_HEIGHT);
        if (folderClosed == NULL) {
            return;
        }
        image_data_to_tiles(folderCloseBuffer, folderClosed, BODYLZ_FOLDER_WIDTH, BODYLZ_FOLDER_HEIGHT, RGB888, dither);
        free_image(folderClosed);

        folderEnabled = true;
    }

    bodylz->flags.folderTexture = folderEnabled;

    if (folderEnabled) {
        bodylz->flags.folderOpenedOffset = openOffset;
        bodylz->flags.folderClosedOffset = closeOffset;
    }
}

static void loadFileTexture(void* fileLargeBuffer, void* fileSmallBuffer, BODYLZ* bodylz, CSimpleIniA* config, u32 fileLargeOffset, u32 fileSmallOffset, bool dither) {
    bool fileEnabled = false;
    const char* fileLargePath = config->GetValue("textures", "filelarge.image");
    const char* fileSmallPath = config->GetValue("textures", "filesmall.image");
    if (fileLargePath != NULL) {
        u8* fileLarge = (u8*) load_image(fileLargePath, BODYLZ_LARGE_FILE_WIDTH, BODYLZ_LARGE_FILE_HEIGHT);
        if (fileLarge == NULL) {
            return;
        }
        image_data_to_tiles(fileLargeBuffer, fileLarge, BODYLZ_LARGE_FILE_WIDTH, BODYLZ_LARGE_FILE_HEIGHT, RGB888, dither);
        free_image(fileLarge);

        fileEnabled = true;
    }

    if (fileSmallPath != NULL) {
        u8* fileSmall = (u8*) load_image(fileSmallPath, BODYLZ_SMALL_FILE_WIDTH, BODYLZ_SMALL_FILE_HEIGHT);
        if (fileSmall == NULL) {
            return;
        }
        image_data_to_tiles(fileSmallBuffer, fileSmall, BODYLZ_SMALL_FILE_WIDTH, BODYLZ_SMALL_FILE_HEIGHT, RGB888, dither);
        free_image(fileSmall);

        fileEnabled = true;
    }

    bodylz->flags.fileTexture = fileEnabled;

    if (fileEnabled) {
        bodylz->flags.fileLargeOffset = fileLargeOffset;
        bodylz->flags.fileSmallOffset = fileSmallOffset;
    }
}

static void loadTopTextures(BODYLZ* bodylz, int topDrawType, int width, void* topTextureBuffer, const char* topPath, u32 topOffset, void* topExtTextureBuffer, const char* topExtPath = "", u32 topExtOffset = 0, bool dither = true) {
    bool hasExtTexture = topExtPath != NULL && (topDrawType == BODYLZ_TOP_SOLID_COLOR || topDrawType == BODYLZ_TOP_SOLID_COLOR_TEXTURE);
    if (topPath != NULL) {
        if (topDrawType == BODYLZ_TOP_SOLID_COLOR_TEXTURE) {
            u8* topImage = (u8*) load_image(topPath, BODYLZ_SOLID_COLOR_SIZE, BODYLZ_SOLID_COLOR_SIZE);
            if (topImage == NULL) {
                return;
            }
            u8* rotated = (u8*)malloc(sizeof(u8) * BODYLZ_SOLID_COLOR_SIZE * BODYLZ_SOLID_COLOR_SIZE * sizeof(u32));
            if (rotated == NULL) {
                free_image(topImage);
                return;
            }
            rotate(topImage, rotated, BODYLZ_SOLID_COLOR_SIZE, BODYLZ_SOLID_COLOR_SIZE);

            image_data_to_tiles(topTextureBuffer, rotated, BODYLZ_SOLID_COLOR_SIZE, BODYLZ_SOLID_COLOR_SIZE, A8, dither);

            //Debug only
            //stbi_write_png("test.png", BODYLZ_SOLID_COLOR_SIZE, BODYLZ_SOLID_COLOR_SIZE, 4, topImage, 0);
            //stbi_write_png("rotate.png", BODYLZ_SOLID_COLOR_SIZE, BODYLZ_SOLID_COLOR_SIZE, 4, rotated, 0);
            free_image(topImage);
            free_image(rotated);
        } else {
            u8* topImage = (u8*) load_image(topPath, width, BODYLZ_HEIGHT);
            if (topImage == NULL) {
                return;
            }
            image_data_to_tiles(topTextureBuffer, topImage, width, BODYLZ_HEIGHT, RGB565, dither);
            free_image(topImage);
        }
        bodylz->flags.top.topTextureOffset = topOffset;
    }

    if (hasExtTexture) {
        u8* topExt = (u8*) load_image(topExtPath, BODYLZ_SOLID_COLOR_SIZE, BODYLZ_SOLID_COLOR_SIZE);
        if (topExt == NULL) {
            return;
        }

        u8* rotated;
        rotated = (u8*)malloc(sizeof(u8) * BODYLZ_SOLID_COLOR_SIZE * BODYLZ_SOLID_COLOR_SIZE * sizeof(u32));
        if (rotated == NULL) {
            free_image(topExt);
            return;
        }
        rotate(topExt, rotated, BODYLZ_SOLID_COLOR_SIZE, BODYLZ_SOLID_COLOR_SIZE);

        image_data_to_tiles(topExtTextureBuffer, rotated, BODYLZ_SOLID_COLOR_SIZE, BODYLZ_SOLID_COLOR_SIZE, A8, dither);

        free_image(topExt);
        free_image(rotated);

        bodylz->flags.top.topTextureExtOffset = topExtOffset;
    }
}

static void loadBottomTexture(void* bottomTextureBuffer, BODYLZ* bodylz, const char* bottomPath, u32 bottomOffset, int width, bool dither) {
    if (bottomPath != NULL) {
        u8* bottomImage = (u8*) load_image(bottomPath, width, BODYLZ_HEIGHT);
        if (bottomImage == NULL) {
            return;
        }
        image_data_to_tiles(bottomTextureBuffer, bottomImage, width, BODYLZ_HEIGHT, RGB565, dither);
        free_image(bottomImage);

        bodylz->flags.bottom.bottomTextureOffset = bottomOffset;
    }
}

static bool verifySFX(BODYLZ* bodylz, CSimpleIniA* config, u32 bodylzSize, u32 texturesSize, BODYLZSfx* sfxData) {
    const char* sfxPaths[] = {
        config->GetValue("audio", "sfx.cursor"),
        config->GetValue("audio", "sfx.launch"),
        config->GetValue("audio", "sfx.folder"),
        config->GetValue("audio", "sfx.close"),
        config->GetValue("audio", "sfx.frame0"),
        config->GetValue("audio", "sfx.frame1"),
        config->GetValue("audio", "sfx.frame2"),
        config->GetValue("audio", "sfx.openlid")
    };

    bool sfxEnabled = false;
    u32 fSize = sizeof(cwavHeader) + sizeof(extData);

    for(int j = 0; j < 8; j++) {
        const char* path = sfxPaths[j];
        if (path != NULL) {
            sfxEnabled = true;
            u32 sfxSize;
            // Sounds horrible...
            //void* cursor = convert_to_cwav(&sfxSize, path, false, 0, 0);

            // Load cwav/bcwav directly
            void* sfx = read_file(&sfxSize, path);
            if (sfx == NULL) {
                perror("ERROR: Cannot read SFX file.");
                return false;
            }

            fSize += sizeof(cwavEnabled) + sizeof(sfxSize) + sfxSize;
            sfxData->cwavs[j].data = sfx;
            sfxData->cwavs[j].size = sfxSize;
            sfxData->cwavs[j].enabled = true;
        } else {
            fSize += sizeof(cwavDisabled) + sizeof(cwavNoSize);
            sfxData->cwavs[j].enabled = false;
        }
    }

    if (sfxEnabled) {
        if (fSize >= 0x2dc00) {
            printf("ERROR: SFX Chunk is too large. Remove some SFXes, or use lower quality ones.");
            return false;
        }
        bodylz->flags.sfx = 1;
        bodylz->flags.cwavOffset = bodylzSize + texturesSize;
        bodylz->flags.cwavLength = fSize;
    }
    return sfxEnabled;
}

static void writeSFX(BODYLZSfx* sfxData, FILE* fd) {
    fwrite(&cwavHeader, 1, sizeof(cwavHeader), fd);

    for(int i = 0; i < 8; i++) {
        if (i == 4) {
            fwrite(&extData, 1, sizeof(extData), fd);
        }
        if (sfxData->cwavs[i].enabled) {
            u32 sfxSize = sfxData->cwavs[i].size;
            void* sfx = sfxData->cwavs[i].data;
            fwrite(&sfxSize, 1, sizeof(sfxSize), fd);
            fwrite(&cwavEnabled, 1, sizeof(cwavEnabled), fd);
            fwrite(sfx, 1, sfxSize, fd);

            free(sfx);
        } else {
            fwrite(&cwavNoSize, 1, sizeof(cwavNoSize), fd);
            fwrite(&cwavDisabled, 1, sizeof(cwavDisabled), fd);
        }
    }
}

static int cmd_make_bodylz(const std::string& path, const std::string& output, bool nocompress, bool enableBGM=false, bool dither=true) {
    std::map<std::string, int> topFrameTypes = {
        {"fastscroll", BODYLZ_TOP_FRAME_FASTSCROLL},
        {"single", BODYLZ_TOP_FRAME_SINGLE},
        {"invalid", BODYLZ_TOP_FRAME_INVALID},
        {"slowscroll", BODYLZ_TOP_FRAME_SLOWSCROLL}
    };
    std::map<std::string, int>  bottomFrameTypes = {
        {"fastscroll", BODYLZ_BOTTOM_FRAME_FASTSCROLL},
        {"single", BODYLZ_BOTTOM_FRAME_SINGLE},
        {"pagescroll", BODYLZ_BOTTOM_FRAME_PAGESCROLL},
        {"slowscroll", BODYLZ_BOTTOM_FRAME_SLOWSCROLL},
        {"bouncescroll", BODYLZ_BOTTOM_FRAME_BOUNCESCROLL}
    };
    std::map<std::string, int> topDrawTypes = {
        {"color", BODYLZ_TOP_SOLID_COLOR},
        {"none", BODYLZ_TOP_NONE},
        {"colortexture", BODYLZ_TOP_SOLID_COLOR_TEXTURE},
        {"texture", BODYLZ_TOP_TEXTURE}
    };
    std::map<std::string, int> bottomDrawTypes = {
        {"color", BODYLZ_BOTTOM_SOLID_COLOR},
        {"none", BODYLZ_BOTTOM_NONE},
        {"texture", BODYLZ_BOTTOM_TEXTURE}
    };

    CSimpleIniA config;
    config.SetUnicode();
    SI_Error rc = config.LoadFile(path.c_str());
    if (rc < 0) {
        perror("ERROR: Configuration file is empty or invalid.");
        return -1;
    }

    const char* topFrame = config.GetValue("frames", "top.frame");
    const char* bottomFrame = config.GetValue("frames", "bottom.frame");
    const char* topType = config.GetValue("frames", "top.type");
    const char* bottomType = config.GetValue("frames", "bottom.type");
    if (topFrame == NULL || bottomFrame == NULL || bottomFrameTypes.find(bottomFrame) == bottomFrameTypes.end()) {
        printf("ERROR: Top/bottom frame type invalid or not specified.\n");
        return -1;
    }

    if (topType == NULL || topDrawTypes.find(topType) == topDrawTypes.end() || bottomType == NULL || bottomDrawTypes.find(bottomType) == bottomDrawTypes.end()) {
        printf("ERROR: Top/bottom draw type invalid or not specified.\n");
        return -1;
    }

    const char* topTexture = config.GetValue("textures", "top.image");
    const char* topExtTexture = config.GetValue("textures", "topext.image");
    const char* bottomTexture = config.GetValue("textures", "bottom.image");
    if (topDrawTypes[topType] == BODYLZ_TOP_TEXTURE || topDrawTypes[topType] == BODYLZ_TOP_SOLID_COLOR_TEXTURE) {
        if (topTexture == NULL) {
            printf("ERROR: Top texture for draw type TEXTURE or SOLID_COLOR_TEXTURE is missing.\n");
            return -1;
        }
    }

    if (bottomDrawTypes[bottomType] == BODYLZ_BOTTOM_TEXTURE) {
        if (bottomTexture == NULL) {
            printf("ERROR: Bottom texture for draw type TEXTURE is missing.\n");
            return -1;
        }
    }

    BODYLZ bodylz;

    memset(&bodylz, 0, sizeof(bodylz));

    bodylz.version = 1;

    const char* bgmPath = config.GetValue("audio", "bgm", NULL);
    if (bgmPath != NULL || enableBGM) {
        bodylz.bgmEnabled = 0x0100;
    }

    bodylz.flags.top.topFrameType = topFrameTypes.find(topFrame) == topFrameTypes.end() ? BODYLZ_TOP_FRAME_INVALID : topFrameTypes[topFrame];
    bodylz.flags.bottom.bottomFrameType = bottomFrameTypes[bottomFrame];

    bodylz.flags.top.topDrawType = topDrawTypes[topType];
    bodylz.flags.bottom.bottomDrawType = bottomDrawTypes.find(bottomType) == bottomDrawTypes.end() ? BODYLZ_BOTTOM_INVALID : bottomDrawTypes[bottomType];

    u32 topWidth = BODYLZ_WIDTH;
    u32 bottomWidth = BODYLZ_WIDTH;

    addColors(&bodylz, &config);

    if (topDrawTypes[topType] == BODYLZ_TOP_SOLID_COLOR_TEXTURE || topDrawTypes[topType] == BODYLZ_TOP_SOLID_COLOR) {
        addTopBGColorParams(&bodylz, &config);
    }

    u32 texturesSize = 0;
    int bodylzType = BODYLZ_WIDE;
    if ((topFrameTypes.find(topFrame) == topFrameTypes.end() || topFrameTypes[topFrame] == BODYLZ_TOP_FRAME_SINGLE) && bottomFrameTypes[bottomFrame] == BODYLZ_BOTTOM_FRAME_SINGLE) {
        //normal bodylz
        bodylzType = BODYLZ_NORMAL;
        BODYLZTextures instance;
        texturesSize = sizeof(instance);
    } else if (topFrameTypes.find(topFrame) == topFrameTypes.end() || topFrameTypes[topFrame] == BODYLZ_TOP_FRAME_SINGLE) {
        //bottom wide bodylz
        bodylzType = BODYLZ_BOTTOM_WIDE;
        bottomWidth = BODYLZ_WIDTH_WIDE;
        BODYLZTexturesBottomWide instanceBottom;
        texturesSize = sizeof(instanceBottom);
    } else if (bottomFrameTypes[bottomFrame] == BODYLZ_BOTTOM_FRAME_SINGLE) {
        //top wide bodylz
        topWidth = BODYLZ_WIDTH_WIDE;
        bodylzType = BODYLZ_TOP_WIDE;
        BODYLZTexturesTopWide instanceTop;
        texturesSize = sizeof(instanceTop);
    } else {
        // wide bodylz
        topWidth = BODYLZ_WIDTH_WIDE;
        bottomWidth = BODYLZ_WIDTH_WIDE;
        BODYLZTexturesWide instanceWide;
        texturesSize = sizeof(instanceWide);
    }

    BODYLZSfx sfxData;
    bool sfxEnabled = verifySFX(&bodylz, &config, sizeof(bodylz), texturesSize, &sfxData);

    FILE* fd = fopen(output.c_str(), "wb");
    if(fd == NULL) {
        perror("ERROR: Could not open file for writing");
        return -1;
    }

    if (bodylzType == BODYLZ_NORMAL) {
        BODYLZTextures textures;

        memset(&textures, 0, sizeof(textures));

        loadTopTextures(&bodylz, topDrawTypes[topType], topWidth, textures.topTexture, topTexture, sizeof(bodylz) + offsetof(BODYLZTextures, topTexture[0]), textures.topExt, topExtTexture, sizeof(bodylz) + offsetof(BODYLZTextures, topExt[0]), dither);
        loadBottomTexture(textures.bottomTexture, &bodylz, bottomTexture, sizeof(bodylz) + offsetof(BODYLZTextures, bottomTexture[0]), bottomWidth, dither);

        loadFolderTexture(textures.folderOpen, textures.folderClose, &bodylz, &config, sizeof(bodylz) + offsetof(BODYLZTextures, folderOpen[0]), sizeof(bodylz) + offsetof(BODYLZTextures, folderClose[0]), dither);
        loadFileTexture(textures.largeFile, textures.smallFile, &bodylz, &config, sizeof(bodylz) + offsetof(BODYLZTextures, largeFile[0]), sizeof(bodylz) + offsetof(BODYLZTextures, smallFile[0]), dither);

        fwrite(&bodylz, 1, sizeof(bodylz), fd);
        fwrite(&textures, 1, sizeof(textures), fd);

        if (sfxEnabled) {
            writeSFX(&sfxData, fd);
        }
    } else if (bodylzType == BODYLZ_BOTTOM_WIDE) {
        BODYLZTexturesBottomWide texturesBottomWide;

        memset(&texturesBottomWide, 0, sizeof(texturesBottomWide));

        loadTopTextures(&bodylz, topDrawTypes[topType], topWidth, texturesBottomWide.topTexture, topTexture, sizeof(bodylz) + offsetof(BODYLZTexturesBottomWide, topTexture[0]), texturesBottomWide.topExt, topExtTexture, sizeof(bodylz) + offsetof(BODYLZTexturesBottomWide, topExt[0]), dither);
        loadBottomTexture(texturesBottomWide.bottomTexture, &bodylz, bottomTexture, sizeof(bodylz) + offsetof(BODYLZTexturesBottomWide, bottomTexture[0]), bottomWidth, dither);

        loadFolderTexture(texturesBottomWide.folderOpen, texturesBottomWide.folderClose, &bodylz, &config, sizeof(bodylz) + offsetof(BODYLZTexturesBottomWide, folderOpen[0]), sizeof(bodylz) + offsetof(BODYLZTexturesBottomWide, folderClose[0]), dither);
        loadFileTexture(texturesBottomWide.largeFile, texturesBottomWide.smallFile, &bodylz, &config, sizeof(bodylz) + offsetof(BODYLZTexturesBottomWide, largeFile[0]), sizeof(bodylz) + offsetof(BODYLZTexturesBottomWide, smallFile[0]), dither);

        fwrite(&bodylz, 1, sizeof(bodylz), fd);
        fwrite(&texturesBottomWide, 1, sizeof(texturesBottomWide), fd);

        if (sfxEnabled) {
            writeSFX(&sfxData, fd);
        }
    } else if (bodylzType == BODYLZ_TOP_WIDE) {
        BODYLZTexturesTopWide texturesTopWide;

        memset(&texturesTopWide, 0, sizeof(texturesTopWide));

        loadTopTextures(&bodylz, topDrawTypes[topType], topWidth, texturesTopWide.topTexture, topTexture, sizeof(bodylz) + offsetof(BODYLZTexturesTopWide, topTexture[0]), texturesTopWide.topExt, topExtTexture, sizeof(bodylz) + offsetof(BODYLZTexturesTopWide, topExt[0]), dither);
        loadBottomTexture(texturesTopWide.bottomTexture, &bodylz, bottomTexture, sizeof(bodylz) + offsetof(BODYLZTexturesTopWide, bottomTexture[0]), bottomWidth, dither);

        loadFolderTexture(texturesTopWide.folderOpen, texturesTopWide.folderClose, &bodylz, &config, sizeof(bodylz) + offsetof(BODYLZTexturesTopWide, folderOpen[0]), sizeof(bodylz) + offsetof(BODYLZTexturesTopWide, folderClose[0]), dither);
        loadFileTexture(texturesTopWide.largeFile, texturesTopWide.smallFile, &bodylz, &config, sizeof(bodylz) + offsetof(BODYLZTexturesTopWide, largeFile[0]), sizeof(bodylz) + offsetof(BODYLZTexturesTopWide, smallFile[0]), dither);

        fwrite(&bodylz, 1, sizeof(bodylz), fd);
        fwrite(&texturesTopWide, 1, sizeof(texturesTopWide), fd);

        if (sfxEnabled) {
            writeSFX(&sfxData, fd);
        }
    } else {
        BODYLZTexturesWide texturesWide;

        memset(&texturesWide, 0, sizeof(texturesWide));

        loadTopTextures(&bodylz, topDrawTypes[topType], topWidth, texturesWide.topTexture, topTexture, sizeof(bodylz) + offsetof(BODYLZTexturesWide, topTexture[0]), texturesWide.topExt, topExtTexture, sizeof(bodylz) + offsetof(BODYLZTexturesWide, topExt[0]), dither);
        loadBottomTexture(texturesWide.bottomTexture, &bodylz, bottomTexture, sizeof(bodylz) + offsetof(BODYLZTexturesWide, bottomTexture[0]), bottomWidth, dither);

        loadFolderTexture(texturesWide.folderOpen, texturesWide.folderClose, &bodylz, &config, sizeof(bodylz) + offsetof(BODYLZTexturesWide, folderOpen[0]), sizeof(bodylz) + offsetof(BODYLZTexturesWide, folderClose[0]), dither);
        loadFileTexture(texturesWide.largeFile, texturesWide.smallFile, &bodylz, &config, sizeof(bodylz) + offsetof(BODYLZTexturesWide, largeFile[0]), sizeof(bodylz) + offsetof(BODYLZTexturesWide, smallFile[0]), dither);

        fwrite(&bodylz, 1, sizeof(bodylz), fd);
        fwrite(&texturesWide, 1, sizeof(texturesWide), fd);

        if (sfxEnabled) {
            writeSFX(&sfxData, fd);
        }
    }

    fclose(fd);

    if (!nocompress) {
        u32 compressedSize = 0;
        u32 uncompressedSize = 0;
        void* uncompressed = read_file(&uncompressedSize, output);
        if (uncompressed == NULL) {
            return 1;
        }
        void* compressed = lz11_compress(&compressedSize, uncompressed, uncompressedSize);

        if(!write_file(compressed, compressedSize, output)) {
            free(uncompressed);
            free(compressed);
            return 1;
        }

        free(uncompressed);
        free(compressed);
    }

    printf("3DS theme file successfully created in \"%s\".\n", output.c_str());
    return 0;
}

static int cmd_lz11(const std::string& input, const std::string& output) {
    u32 size = 0;
    void* data = read_file(&size, input);
    if(data == NULL) {
        return 1;
    }

    u32 compressedSize = 0;
    void* compressed = lz11_compress(&compressedSize, data, size);

    free(data);

    if(compressed == NULL || !write_file(compressed, compressedSize, output)) {
        return 1;
    }

    printf("Compressed to file \"%s\".\n", output.c_str());
    return 0;
}

static int cmd_decompresslz11(const std::string& input, const std::string& output) {
    u32 size = 0;
    u8* data = (u8*)read_file(&size, input);
    if(data == NULL) {
        return 1;
    }

    u32 decompressedSize = lz11_decompressed_size(data);
    u8* decompressed = (u8*)malloc(decompressedSize);
    if(!decompressed) {
        return 1;
    }
    lz11_decompress(data+4, decompressed, decompressedSize);

    free(data);

    if(!write_file(decompressed, decompressedSize, output)) {
        free(decompressed);
        return 1;
    }

    printf("Decompressed to file \"%s\".\n", output.c_str());
    free(decompressed);
    return 0;
}

#if defined _WIN32 && !defined __CYGWIN__
#define WinStartPathCheck(x) (x.length() >= 3 && x.find_first_of("/\\") == 2 && x.at(1) == ':' && (x.at(0)&0xDF) >= 'A' && (x.at(0)&0xDF) <= 'Z')
#else
#define WinStartPathCheck(x) false
#endif

#if defined _WIN32 && !defined __CYGWIN__
    #define STR_TOKEN "\\"
    #define LAST_FOLDER "..\\"
    #define FOLDER_SEP "\\"
    #define LINE_BREAK "\r\n"
#else
    #define STR_TOKEN "/"
    #define LAST_FOLDER "../"
    #define FOLDER_SEP "/"
    #define LINE_BREAK "\n"
#endif // _WIN32

#ifdef WIN32
#define IS_SLASH(s) ((s == '/') || (s == '\\'))
#else
#define IS_SLASH(s) (s == '/')
#endif

static void change_directory(std::string &output, std::string &config) {
    std::string oldConfig = config;
    size_t found = config.find_last_of("/\\");
    if (found >= 0) {
            char buffer[4095];
            char* cwd = getcwd(buffer, sizeof(buffer));
            std::string currentDirectory(cwd);
            if (output.at(0) != '/' && !WinStartPathCheck(output)) {
                output = currentDirectory + STR_TOKEN + output;
            }
            if (config.at(0) != '/' && !WinStartPathCheck(config)) {
                config = currentDirectory + STR_TOKEN + config;
            }
        chdir(oldConfig.substr(0, found).c_str());
    }
}

std::string getRcPath(std::string resourcepath, std::string output) {
    char* configPath = (char*)resourcepath.c_str();
    char* newPath = (char*)output.c_str();

    apathy::Path relativePath = apathy::Path("");
    std::vector<apathy::Path::Segment> configParts = apathy::Path(newPath).absolute().sanitize().trim().split();
    std::vector<apathy::Path::Segment> targetParts = apathy::Path(configPath).absolute().sanitize().trim().split();
    long unsigned int index = 0;
    long unsigned int i = 0;
    bool indexFound = false;
    for(apathy::Path::Segment part : configParts) {
        if (!indexFound && !part.segment.empty() && i < targetParts.size() && strcmp(part.segment.c_str(), targetParts.at(i).segment.c_str()) != 0) {
            index = i;
            indexFound = true;
        } if (indexFound && i < configParts.size() - 1) {
            relativePath << LAST_FOLDER;
        } if (!indexFound && i > targetParts.size()) {
            relativePath.up();
        }
        i++;
    }

    i = 0;
    if (indexFound) {
        for(apathy::Path::Segment part : targetParts) {
            if (i >= index) {
                relativePath << part.segment;
            }
            i++;
        }
        std::string result = relativePath.string().substr(1, std::string::npos);
        if (!IS_SLASH(result.back())) {
            result += STR_TOKEN;
        }
        return result;
    }
    return relativePath.string();
}

static std::map<std::string, std::string> cmd_get_args(int argc, char* argv[]) {
    std::map<std::string, std::string> args;
    for(int i = 0; i < argc; i++) {
        if((strncmp(argv[i], "-", 1) == 0 || strncmp(argv[i], "--", 2) == 0) && argc != i + 1) {
            args[argv[i]] = argv[i + 1];
            i++;
        }
    }

    return args;
}

static std::string cmd_find_arg(const std::map<std::string, std::string>& args, const std::string& shortOpt, const std::string& longOpt, const std::string& def) {
    std::string sopt = "-" + shortOpt;
    std::string lopt = "--" + longOpt;

    std::map<std::string, std::string>::const_iterator match = args.find(sopt);
    if(match != args.end()) {
        return (*match).second;
    }

    match = args.find(lopt);
    if(match != args.end()) {
        return (*match).second;
    }

    return def;
}

static std::vector<std::string> cmd_parse_list(const std::string& list) {
    std::vector<std::string> ret;
    std::string::size_type lastPos = 0;
    std::string::size_type pos = 0;
    while((pos = list.find(',', lastPos)) != std::string::npos) {
        ret.push_back(list.substr(lastPos, pos - lastPos));
        lastPos = pos + 1;
    }

    if(lastPos < list.size()) {
        ret.push_back(list.substr(lastPos));
    }

    return ret;
}

static void cmd_print_info(const std::string& command) {
    if(command.compare("makebanner") == 0) {
        printf("makebanner - Creates a .bnr file.\n");
        printf("  -i/--image: Optional if specified for a language or with a CGFX. PNG file to use as the banner's default image.\n");
        printf("    -eei/--eurenglishimage: Optional if default or CGFX specified. PNG file to use as the banner's EUR English image.\n");
        printf("    -efi/--eurfrenchimage: Optional if default or CGFX specified. PNG file to use as the banner's EUR French image.\n");
        printf("    -egi/--eurgermanimage: Optional if default or CGFX specified. PNG file to use as the banner's EUR German image.\n");
        printf("    -eii/--euritalianimage: Optional if default or CGFX specified. PNG file to use as the banner's EUR Italian image.\n");
        printf("    -esi/--eurspanishimage: Optional if default or CGFX specified. PNG file to use as the banner's EUR Spanish image.\n");
        printf("    -edi/--eurdutchimage: Optional if default or CGFX specified. PNG file to use as the banner's EUR Dutch image.\n");
        printf("    -epi/--eurportugueseimage: Optional if default or CGFX specified. PNG file to use as the banner's EUR Portuguese image.\n");
        printf("    -eri/--eurrussianimage: Optional if default or CGFX specified. PNG file to use as the banner's EUR Russian image.\n");
        printf("    -jji/--jpnjapaneseimage: Optional if default or CGFX specified. PNG file to use as the banner's JPN Japanese image.\n");
        printf("    -uei/--usaenglishimage: Optional if default or CGFX specified. PNG file to use as the banner's USA English image.\n");
        printf("    -ufi/--usafrenchimage: Optional if default or CGFX specified. PNG file to use as the banner's USA French image.\n");
        printf("    -usi/--usaspanishimage: Optional if default or CGFX specified. PNG file to use as the banner's USA Spanish image.\n");
        printf("    -upi/--usaportugueseimage: Optional if default or CGFX specified. PNG file to use as the banner's USA Portuguese image.\n");
        printf("  -ci/--cgfximage: Optional if specified for a language or with a PNG. CGFX file to use as the banner's default image.\n");
        printf("    -eeci/--eurenglishcgfximage: Optional if default or PNG specified. CGFX file to use as the banner's EUR English image.\n");
        printf("    -efci/--eurfrenchcgfximage: Optional if default or PNG specified. CGFX file to use as the banner's EUR French image.\n");
        printf("    -egci/--eurgermancgfximage: Optional if default or PNG specified. CGFX file to use as the banner's EUR German image.\n");
        printf("    -eici/--euritaliancgfximage: Optional if default or PNG specified. CGFX file to use as the banner's EUR Italian image.\n");
        printf("    -esci/--eurspanishcgfximage: Optional if default or PNG specified. CGFX file to use as the banner's EUR Spanish image.\n");
        printf("    -edci/--eurdutchcgfximage: Optional if default or PNG specified. CGFX file to use as the banner's EUR Dutch image.\n");
        printf("    -epci/--eurportuguesecgfximage: Optional if default or PNG specified. CGFX file to use as the banner's EUR Portuguese image.\n");
        printf("    -erci/--eurrussiancgfximage: Optional if default or PNG specified. CGFX file to use as the banner's EUR Russian image.\n");
        printf("    -jjci/--jpnjapanesecgfximage: Optional if default or PNG specified. CGFX file to use as the banner's JPN Japanese image.\n");
        printf("    -ueci/--usaenglishcgfximage: Optional if default or PNG specified. CGFX file to use as the banner's USA English image.\n");
        printf("    -ufci/--usafrenchcgfximage: Optional if default or PNG specified. CGFX file to use as the banner's USA French image.\n");
        printf("    -usci/--usaspanishcgfximage: Optional if default or PNG specified. CGFX file to use as the banner's USA Spanish image.\n");
        printf("    -upci/--usaportuguesecgfximage: Optional if default or PNG specified. CGFX file to use as the banner's USA Portuguese image.\n");
        printf("  -a/--audio: Optional if specified for a language or with a CWAV. WAV/OGG file to use as the banner's tune.\n");
        printf("  -ca/--cwavaudio: Optional if specified for a language or with a WAV/OGG. CWAV file to use as the banner's tune.\n");
        printf("  -o/--output: File to output the created banner to.\n");
    } else if(command.compare("makesmdh") == 0) {
        printf("makesmdh - Creates a .smdh/.icn file.\n");
        printf("  -s/--shorttitle: Optional if specified for a language. Default short title of the application.\n");
        printf("    -js/--japaneseshorttitle: Optional if default specified. Japanese short title of the application.\n");
        printf("    -es/--englishshorttitle: Optional if default specified. English short title of the application.\n");
        printf("    -fs/--frenchshorttitle: Optional if default specified. French short title of the application.\n");
        printf("    -gs/--germanshorttitle: Optional if default specified. German short title of the application.\n");
        printf("    -is/--italianshorttitle: Optional if default specified. Italian short title of the application.\n");
        printf("    -ss/--spanishshorttitle: Optional if default specified. Spanish short title of the application.\n");
        printf("    -scs/--simplifiedchineseshorttitle: Optional if default specified. Simplified Chinese short title of the application.\n");
        printf("    -ks/--koreanshorttitle: Optional if default specified. Korean short title of the application.\n");
        printf("    -ds/--dutchshorttitle: Optional if default specified. Dutch short title of the application.\n");
        printf("    -ps/--portugueseshorttitle: Optional if default specified. Portuguese short title of the application.\n");
        printf("    -rs/--russianshorttitle: Optional if default specified. Russian short title of the application.\n");
        printf("    -tcs/--traditionalchineseshorttitle: Optional if default specified. Traditional Chinese short title of the application.\n");
        printf("  -l/--longtitle: Optional if specified for a language. Default long title of the application.\n");
        printf("    -jl/--japaneselongtitle: Optional if default specified. Japanese long title of the application.\n");
        printf("    -el/--englishlongtitle: Optional if default specified. English long title of the application.\n");
        printf("    -fl/--frenchlongtitle: Optional if default specified. French long title of the application.\n");
        printf("    -gl/--germanlongtitle: Optional if default specified. German long title of the application.\n");
        printf("    -il/--italianlongtitle: Optional if default specified. Italian long title of the application.\n");
        printf("    -sl/--spanishlongtitle: Optional if default specified. Spanish long title of the application.\n");
        printf("    -scl/--simplifiedchineselongtitle: Optional if default specified. Simplified Chinese long title of the application.\n");
        printf("    -kl/--koreanlongtitle: Optional if default specified. Korean long title of the application.\n");
        printf("    -dl/--dutchlongtitle: Optional if default specified. Dutch long title of the application.\n");
        printf("    -pl/--portugueselongtitle: Optional if default specified. Portuguese long title of the application.\n");
        printf("    -rl/--russianlongtitle: Optional if default specified. Russian long title of the application.\n");
        printf("    -tcl/--traditionalchineselongtitle: Optional if default specified. Traditional Chinese long title of the application.\n");
        printf("  -p/--publisher: Optional if specified for a language. Default publisher of the application.\n");
        printf("    -jp/--japanesepublisher: Optional if default specified. Japanese publisher of the application.\n");
        printf("    -ep/--englishpublisher: Optional if default specified. English publisher of the application.\n");
        printf("    -fp/--frenchpublisher: Optional if default specified. French publisher of the application.\n");
        printf("    -gp/--germanpublisher: Optional if default specified. German publisher of the application.\n");
        printf("    -ip/--italianpublisher: Optional if default specified. Italian publisher of the application.\n");
        printf("    -sp/--spanishpublisher: Optional if default specified. Spanish publisher of the application.\n");
        printf("    -scp/--simplifiedchinesepublisher: Optional if default specified. Simplified Chinese publisher of the application.\n");
        printf("    -kp/--koreanpublisher: Optional if default specified. Korean publisher of the application.\n");
        printf("    -dp/--dutchpublisher: Optional if default specified. Dutch publisher of the application.\n");
        printf("    -pp/--portuguesepublisher: Optional if default specified. Portuguese publisher of the application.\n");
        printf("    -rp/--russianpublisher: Optional if default specified. Russian publisher of the application.\n");
        printf("    -tcp/--traditionalchinesepublisher: Optional if default specified. Traditional Chinese publisher of the application.\n");
        printf("  -i/--icon: PNG file to use as an icon.\n");
        printf("  -si/--small-icon: Optional. PNG file to use as the small icon. Defaults to the icon provided by the flag above if this is not provided.\n");
        printf("  -o/--output: File to output the created SMDH/ICN to.\n");
        printf("  -r/--regions: Optional. Comma separated list of regions to lock the SMDH to.\n");
        printf("     Valid regions: regionfree, japan, northamerica, europe, australia, china, korea, taiwan.\n");
        printf("  -f/--flags: Optional. Flags to apply to the SMDH file.\n");
        printf("     Valid flags: visible, autoboot, allow3d, requireeula, autosave, extendedbanner, ratingrequired, savedata, recordusage, nosavebackups, new3ds.\n");
        printf("  -mmid/--matchmakerid: Optional. Match maker ID of the SMDH.\n");
        printf("  -mmbid/--matchmakerbitid: Optional. Match maker BIT ID of the SMDH.\n");
        printf("  -ev/--eulaversion: Optional. Version of the EULA required to be accepted before launching.\n");
        printf("  -obf/--optimalbannerframe: Optional. Optimal frame of the accompanying banner.\n");
        printf("  -spid/--streetpassid: Optional. Streetpass ID of the SMDH.\n");
        printf("  -cer/--cero: Optional. CERO rating number (0-255).\n");
        printf("  -er/--esrb: Optional. ESRB rating number (0-255).\n");
        printf("  -ur/--usk: Optional. USK rating number (0-255).\n");
        printf("  -pgr/--pegigen: Optional. PEGI GEN rating number (0-255).\n");
        printf("  -ppr/--pegiptr: Optional. PEGI PTR rating number (0-255).\n");
        printf("  -pbr/--pegibbfc: Optional. PEGI BBFC rating number (0-255).\n");
        printf("  -cr/--cob: Optional. COB rating number (0-255).\n");
        printf("  -gr/--grb: Optional. GR rating number (0-255).\n");
        printf("  -cgr/--cgsrr: Optional. CGSRR rating number (0-255).\n");
    } else if(command.compare("makecwav") == 0) {
        printf("makecwav - Creates a CWAV file from a WAV.\n");
        printf("  -i/--input: WAV file to convert.\n");
        printf("  -o/--output: File to output the created CWAV to.\n");
        printf("  -l/--loop: Optional. Whether or not the audio should loop (false/true).\n");
        printf("  -s/--loopstartframe: Optional. Sample frame to return to when looping.\n");
        printf("  -f/--loopendframe: Optional. Sample frame to loop at.\n");
    } else if(command.compare("lz11") == 0) {
        printf("lz11 - Compresses a file with LZ11.\n");
        printf("  -i/--input: File to compress.\n");
        printf("  -o/--output: File to output the compressed data to.\n");
    } else if(command.compare("decompresslz11") == 0) {
        printf("decompresslz11 - Decompress a file with LZ11.\n");
        printf("  -i/--input: File to decompress.\n");
        printf("  -o/--output: File to output the decompressed data to.\n");
    } else if(command.compare("makebodylz") == 0) {
        printf("makebodylz - Create body_lz.bin for 3DS themes.\n");
        printf("  -c/--config: Config file to use.\n");
        printf("  -o/--output: File to output the data to.\n");
        printf("  -n/--nocompress: Optional. Don't compress the output.\n");
        printf("  -nd/--nodither: Optional. Don't dither images.\n");
    } else if(command.compare("make3dstheme") == 0) {
        printf("make3dstheme - Create a 3ds theme archive.\n");
        printf("  -c/--config: Config file to use.\n");
        printf("  -o/--output: File/Folder to output archive to.\n");
        printf("  -n/--nozip: Optional. Output theme files to a folder instead of a zip archive.\n");
        printf("  -nd/--nodither: Optional. Don't dither images.\n");
    } else if (command.compare("extractsmdh") == 0) {
        printf("extractsmdh - Extract data from smdh info file.\n");
        printf("  -i/--input: Smdh file to extract from.\n");
        printf("  -o/--output: Location of the extracted rc file.\n");
        printf("  -r/--resources: Folder where smdh resources should be saved to.\n");
        printf("  -nd/--nodither: Optional. Don't dither images.\n");
    } else if (command.compare("extractbodylz") == 0) {
        printf("extractbodylz - Extract data from body_lz.bin file.\n");
        printf("  -i/--input: body_lz.bin file to extract from.\n");
        printf("  -o/--output: Location of the extracted rc file.\n");
        printf("  -r/--resources: Folder where theme resources should be saved to.\n");
        printf("  -b/--bgm: Optional. path of the bgm bcstm file, if provided.\n");
    } else if (command.compare("extract3dstheme") == 0) {
        printf("extract3dstheme - Extract resources from theme .zip archive.\n");
        printf("  -i/--input: .zip theme archive/folder to extract from.\n");
        printf("  -o/--output: .rc file to extract theme configuration to.\n");
        printf("  -r/--resources: Folder where theme resources should be saved to.\n");
        printf("  -f/--folder: Optional. Indicates the input path is a folder and not a zip archive.\n");
    } else if (command.compare("convertrgb565") == 0) {
        printf("convertrgb565 - Convert an RGB888 image to RGB565. Alpha channels will be ignored.\n");
        printf("  -i/--input: input jpeg or png image.\n");
        printf("  -o/--output: path to output converted image. Images will be converted to png.\n");
        printf("  -nd/--nodither: Optional. Don't dither converted image.\n");
    } else if(command.compare("version") == 0) {
        printf("-v/--version - Print the version number.\n");
    }
}

static void cmd_print_commands() {
    printf("Available commands:\n");
    cmd_print_info("version");
    cmd_print_info("makebanner");
    cmd_print_info("make3dstheme");
    cmd_print_info("makebodylz");
    cmd_print_info("makesmdh");
    cmd_print_info("makecwav");
    cmd_print_info("extractsmdh");
    cmd_print_info("extractbodylz");
    cmd_print_info("extract3dstheme");
    cmd_print_info("convertrgb565");
    cmd_print_info("lz11");
    cmd_print_info("decompresslz11");
}

static void cmd_print_version() {
    printf("kame-tools v%d.%d.%d\n", VERSION_MAJOR, VERSION_MINOR, VERSION_MICRO);
}

static void cmd_print_usage(const std::string& executedFrom) {
    cmd_print_version();
    printf("Usage: %s <command> <args>\n", executedFrom.c_str());
    cmd_print_commands();
}

static void cmd_missing_args(const std::string& command) {
    printf("Missing arguments for command \"%s\".\n", command.c_str());
    cmd_print_info(command);
}

static void cmd_invalid_arg(const std::string& argument, const std::string& command) {
    printf("Invalid value for argument \"%s\" in command \"%s\".\n", argument.c_str(), command.c_str());
    cmd_print_info(command);
}

static void cmd_invalid_command(const std::string& command) {
    printf("Invalid command \"%s\".\n", command.c_str());
    cmd_print_commands();
}

static int make_smdh(std::map<std::string, std::string> args, std::string command) {
    const std::string icon = cmd_find_arg(args, "i", "icon", "");
    std::string smallicon = cmd_find_arg(args, "si", "smallicon", "");
    smallicon = smallicon.empty() ? icon : smallicon;
    const std::string output = cmd_find_arg(args, "o", "output", "");
    const std::string nodither = cmd_find_arg(args, "nd", "nodither", "");
    if(icon.empty() || output.empty()) {
        cmd_missing_args(command);
        return -1;
    }

    SMDH smdh;
    memset(&smdh, 0, sizeof(smdh));

    memcpy(smdh.magic, SMDH_MAGIC, sizeof(smdh.magic));

    static const char* shortTitleShortArgs[SMDH_NUM_VALID_LANGUAGE_SLOTS] = {"js", "es", "fs", "gs", "is", "ss", "scs", "ks", "ds", "ps", "rs", "tcs"};
    static const char* shortTitleLongArgs[SMDH_NUM_VALID_LANGUAGE_SLOTS] = {"japaneseshorttitle", "englishshorttitle", "frenchshorttitle", "germanshorttitle", "italianshorttitle", "spanishshorttitle", "simplifiedchineseshorttitle", "koreanshorttitle", "dutchshorttitle", "portugueseshorttitle", "russianshorttitle", "traditionalchineseshorttitle"};
    static const char* longTitleShortArgs[SMDH_NUM_VALID_LANGUAGE_SLOTS] = {"jl", "el", "fl", "gl", "il", "sl", "scl", "kl", "dl", "pl", "rl", "tcl"};
    static const char* longTitleLongArgs[SMDH_NUM_VALID_LANGUAGE_SLOTS] = {"japaneselongtitle", "englishlongtitle", "frenchlongtitle", "germanlongtitle", "italianlongtitle", "spanishlongtitle", "simplifiedchineselongtitle", "koreanlongtitle", "dutchlongtitle", "portugueselongtitle", "russianlongtitle", "traditionalchineselongtitle"};
    static const char* publisherShortArgs[SMDH_NUM_VALID_LANGUAGE_SLOTS] = {"jp", "ep", "fp", "gp", "ip", "sp", "scp", "kp", "dp", "pp", "rp", "tcp"};
    static const char* publisherLongArgs[SMDH_NUM_VALID_LANGUAGE_SLOTS] = {"japanesepublisher", "englishpublisher", "frenchpublisher", "germanpublisher", "italianpublisher", "spanishpublisher", "simplifiedchinesepublisher", "koreanpublisher", "dutchpublisher", "portuguesepublisher", "russianpublisher", "traditionalchinesepublisher"};

    const std::string shortTitle = cmd_find_arg(args, "s", "shorttitle", "");
    const std::string longTitle = cmd_find_arg(args, "l", "longtitle", "");
    const std::string publisher = cmd_find_arg(args, "p", "publisher", "");

    bool shortTitleFound = false;
    bool longTitleFound = false;
    bool publisherFound = false;

    for(u32 i = 0; i < SMDH_NUM_LANGUAGE_SLOTS; i++) {
        std::string currShortTitle = shortTitle;
        std::string currLongTitle = longTitle;
        std::string currPublisher = publisher;

        if(i < SMDH_NUM_VALID_LANGUAGE_SLOTS) {
            currShortTitle = cmd_find_arg(args, shortTitleShortArgs[i], shortTitleLongArgs[i], shortTitle);
            currLongTitle = cmd_find_arg(args, longTitleShortArgs[i], longTitleLongArgs[i], longTitle);
            currPublisher = cmd_find_arg(args, publisherShortArgs[i], publisherLongArgs[i], publisher);
        }

        shortTitleFound = shortTitleFound || !currShortTitle.empty();
        longTitleFound = longTitleFound || !currLongTitle.empty();
        publisherFound = publisherFound || !currPublisher.empty();

        utf8_to_utf16(smdh.titles[i].shortTitle, currShortTitle, sizeof(smdh.titles[i].shortTitle));
        utf8_to_utf16(smdh.titles[i].longTitle, currLongTitle, sizeof(smdh.titles[i].longTitle));
        utf8_to_utf16(smdh.titles[i].publisher, currPublisher, sizeof(smdh.titles[i].publisher));
    }

    if(!shortTitleFound || !longTitleFound || !publisherFound) {
        cmd_missing_args(command);
        return -1;
    }

    std::vector<std::string> regions = cmd_parse_list(cmd_find_arg(args, "r", "regions", "regionfree"));
    for(std::vector<std::string>::iterator it = regions.begin(); it != regions.end(); it++) {
        const std::string region = *it;
        if(region.compare("regionfree") == 0) {
            smdh.settings.regionLock = SMDH_REGION_FREE;
            break;
        } else if(region.compare("japan") == 0) {
            smdh.settings.regionLock |= SMDH_REGION_JAPAN;
        } else if(region.compare("northamerica") == 0) {
            smdh.settings.regionLock |= SMDH_REGION_NORTH_AMERICA;
        } else if(region.compare("europe") == 0) {
            smdh.settings.regionLock |= SMDH_REGION_EUROPE;
        } else if(region.compare("australia") == 0) {
            smdh.settings.regionLock |= SMDH_REGION_AUSTRALIA;
        } else if(region.compare("china") == 0) {
            smdh.settings.regionLock |= SMDH_REGION_CHINA;
        } else if(region.compare("korea") == 0) {
            smdh.settings.regionLock |= SMDH_REGION_KOREA;
        } else if(region.compare("taiwan") == 0) {
            smdh.settings.regionLock |= SMDH_REGION_TAIWAN;
        } else {
            cmd_invalid_arg("regions", command);
            return -1;
        }
    }

    std::vector<std::string> flags = cmd_parse_list(cmd_find_arg(args, "f", "flags", "visible,allow3d,recordusage"));
    for(std::vector<std::string>::iterator it = flags.begin(); it != flags.end(); it++) {
        const std::string flag = *it;
        if(flag.compare("visible") == 0) {
            smdh.settings.flags |= SMDH_FLAG_VISIBLE;
        } else if(flag.compare("autoboot") == 0) {
            smdh.settings.flags |= SMDH_FLAG_AUTO_BOOT;
        } else if(flag.compare("allow3d") == 0) {
            smdh.settings.flags |= SMDH_FLAG_ALLOW_3D;
        } else if(flag.compare("requireeula") == 0) {
            smdh.settings.flags |= SMDH_FLAG_REQUIRE_EULA;
        } else if(flag.compare("autosave") == 0) {
            smdh.settings.flags |= SMDH_FLAG_AUTO_SAVE_ON_EXIT;
        } else if(flag.compare("extendedbanner") == 0) {
            smdh.settings.flags |= SMDH_FLAG_USE_EXTENDED_BANNER;
        } else if(flag.compare("ratingrequired") == 0) {
            smdh.settings.flags |= SMDH_FLAG_RATING_REQUIED;
        } else if(flag.compare("savedata") == 0) {
            smdh.settings.flags |= SMDH_FLAG_USE_SAVE_DATA;
        } else if(flag.compare("recordusage") == 0) {
            smdh.settings.flags |= SMDH_FLAG_RECORD_USAGE;
        } else if(flag.compare("nosavebackups") == 0) {
            smdh.settings.flags |= SMDH_FLAG_DISABLE_SAVE_BACKUPS;
        } else if(flag.compare("new3ds") == 0) {
            smdh.settings.flags |= SMDH_FLAG_NEW_3DS;
        } else {
            cmd_invalid_arg("flags", command);
            return -1;
        }
    }

    smdh.settings.matchMakerId = (u32) atoi(cmd_find_arg(args, "mmid", "matchmakerid", "0").c_str());
    smdh.settings.matchMakerBitId = (u64) atoll(cmd_find_arg(args, "mmbid", "matchmakerbitid", "0").c_str());

    smdh.settings.eulaVersion = (u16) atoi(cmd_find_arg(args, "ev", "eulaversion", "0").c_str());
    ufloat obf;
    obf.f = atof(cmd_find_arg(args, "obf", "optimalbannerframe", "0").c_str());
    smdh.settings.optimalBannerFrame = obf.u;
    smdh.settings.streetpassId = (u32) atoi(cmd_find_arg(args, "spid", "streetpassid", "0").c_str());

    smdh.settings.gameRatings[SMDH_RATING_CERO] = (u8) atoi(cmd_find_arg(args, "cer", "cero", "0").c_str());
    smdh.settings.gameRatings[SMDH_RATING_ESRB] = (u8) atoi(cmd_find_arg(args, "er", "esrb", "0").c_str());
    smdh.settings.gameRatings[SMDH_RATING_USK] = (u8) atoi(cmd_find_arg(args, "ur", "usk", "0").c_str());
    smdh.settings.gameRatings[SMDH_RATING_PEGI_GEN] = (u8) atoi(cmd_find_arg(args, "pgr", "pegigen", "0").c_str());
    smdh.settings.gameRatings[SMDH_RATING_PEGI_PTR] = (u8) atoi(cmd_find_arg(args, "ppr", "pegiptr", "0").c_str());
    smdh.settings.gameRatings[SMDH_RATING_PEGI_BBFC] = (u8) atoi(cmd_find_arg(args, "pbr", "pegibbfc", "0").c_str());
    smdh.settings.gameRatings[SMDH_RATING_COB] = (u8) atoi(cmd_find_arg(args, "cor", "cob", "0").c_str());
    smdh.settings.gameRatings[SMDH_RATING_GRB] = (u8) atoi(cmd_find_arg(args, "gr", "grb", "0").c_str());
    smdh.settings.gameRatings[SMDH_RATING_CGSRR] = (u8) atoi(cmd_find_arg(args, "cgr", "cgsrr", "0").c_str());

    return cmd_make_smdh(smdh, icon, smallicon, output, nodither.empty());
}

static int cmd_make_3dstheme(const std::string& output, const std::string& path, bool nozip, bool dither) {
    CSimpleIniA config;
    config.SetUnicode();
    SI_Error rc = config.LoadFile(path.c_str());
    if (rc < 0) {
        perror("ERROR: Configuration file is empty or invalid.");
        return -1;
    }

    const char* shorttitle = config.GetValue("info", "shorttitle", "");
    const char* longtitle = config.GetValue("info", "longtitle", "");
    const char* publisher = config.GetValue("info", "publisher", "");
    const char* iconPath = config.GetValue("info", "icon", "");

    if (shorttitle == NULL || longtitle == NULL || publisher == NULL || iconPath == NULL) {
        printf("ERROR: One of shorttitle, longtitle, publisher or icon is missing in the config file.");
        return -1;
    }

    CSimpleIniA::TNamesDepend keys;
    config.GetAllKeys("info", keys);

    std::map<std::string, std::string> infoParams;

    size_t found = output.find_last_of("/\\");
    std::string outputPath = nozip ? output : output.substr(0, found);

    const char* bgmPath = config.GetValue("audio", "bgm", "");
    bool bgmEnabled = false;
    u32 bgmSize = 0;
    void* bgm = NULL;
    if (bgmPath != NULL && bgmPath[0]) {
        bgm = read_file(&bgmSize, bgmPath);

        if (!bgm) {
            printf("WARNING: BGM failed to load, skipping.\n");
        } else if (bgmSize > 3371008) {
            printf("WARNING: BGM included is over 3.3MB. Please optimize your bgm file before including it. BGM will not be included this time.\n");
        } else {
            bgmEnabled = true;
            write_file(bgm, bgmSize, outputPath + "/bgm.bcstm");
        }
    }

    CSimpleIniA::TNamesDepend::const_iterator i;
    for (i = keys.begin(); i != keys.end(); ++i) {
        std::string key(i->pItem);
        infoParams["--" + key] = config.GetValue("info", i->pItem, "");
    }
    infoParams["--output"] = outputPath + "/info.smdh";
    if (!dither) {
        infoParams["--nodither"] = "1";
    }

    int ret = make_smdh(infoParams, "make3dstheme");
    if (ret != 0) {
        free(bgm);
        printf("ERROR: Failed to create info.smdh.\n");
        return -1;
    }
    ret = cmd_make_bodylz(path, outputPath + "/body_lz.bin", false, bgmEnabled, dither);
    if (ret != 0) {
        free(bgm);
        printf("ERROR: Failed to create body_lz.bin.\n");
        return -1;
    }


    if (!nozip) {
        u32 smdhSize;
        u32 bodylzSize;

        void* smdhFile = read_file(&smdhSize, outputPath + "/info.smdh");
        if (!smdhFile) {
            free(bgm);
            printf("ERROR: Failed to read info.smdh.\n");
            return -1;
        }
        void* bodylzFile = read_file(&bodylzSize, outputPath + "/body_lz.bin");
        if (!bodylzFile) {
            free(bgm);
            free(smdhFile);
            printf("ERROR: Failed to read body_lz.bin.\n");
            return -1;
        }

        remove(output.c_str());

        mz_bool status;

        status = mz_zip_add_mem_to_archive_file_in_place(output.c_str(), "info.smdh", smdhFile, smdhSize, "no comment", (u16)strlen("no comment"), MZ_BEST_COMPRESSION);
        if (!status)
        {
            free(bgm);
            free(smdhFile);
            free(bodylzFile);
            printf("ERROR: Failed to create theme zip archive.\n");
            return -1;
        }
        status = mz_zip_add_mem_to_archive_file_in_place(output.c_str(), "body_lz.bin", bodylzFile, bodylzSize, "no comment", (u16)strlen("no comment"), MZ_BEST_COMPRESSION);
        if (!status)
        {
            free(bgm);
            free(smdhFile);
            free(bodylzFile);
            printf("ERROR: Failed to create theme zip archive.\n");
            return -1;
        }
        if (bgmEnabled) {
            status = mz_zip_add_mem_to_archive_file_in_place(output.c_str(), "bgm.bcstm", bgm, bgmSize, "no comment", (u16)strlen("no comment"), MZ_BEST_COMPRESSION);
            if (!status)
            {
                free(bgm);
                free(smdhFile);
                free(bodylzFile);
                printf("ERROR: Failed to create theme zip archive.\n");
                return -1;
            }
        }

        free(smdhFile);
        free(bodylzFile);

        remove((outputPath + "/info.smdh").c_str());
        remove((outputPath + "/body_lz.bin").c_str());
    }

    if (bgmEnabled) {
        free(bgm);
        char buffer[4095];
        char* cwd = getcwd(buffer, sizeof(buffer));
        if (!nozip && cwd != outputPath) {
            remove((outputPath + "/bgm.bcstm").c_str());
        }
    }

    printf("3DS theme archive successfully created at %s.\n", output.c_str());
    return 0;
}

static int extract_image(std::string path, u32 width, u32 height, void* data, PixelFormat format, int rotation = 0) {
    u32 colorsize = formatsize(format);

    u8* textureData = (u8*)malloc(width * height * colorsize);
    u8* texture = (u8*)malloc(width * height * sizeof(RGBColors));
    if (!textureData || !texture) {
        free(textureData);
        free(texture);
        return 1;
    }
    memcpy(textureData, data, width * height * colorsize);
    tiles_to_data(texture, textureData, width, height, format);
    if (rotation != 0) {
        u8* rotated = (u8*)malloc(width * height * sizeof(RGBColors));
        if (rotation > 0) {
            rotate(texture, rotated, width, height, RGB888);
        } else {
            inverse_rotate(texture, rotated, width, height, RGB888);
        }
        if (!stbi_write_png(path.c_str(), width, height, 3, rotated, 0)) {
            free(rotated);
            free(textureData);
            free(texture);
            return 1;
        }
    } else {
        if (!stbi_write_png(path.c_str(), width, height, 3, texture, 0)) {
            free(textureData);
            free(texture);
            return 1;
        }
    }

    free(textureData);
    free(texture);
    return 0;
}

static int cmd_extract_smdh(std::string resourcepath, std::string output, std::string input, CSimpleIniA *config) {
    static const char* languages[SMDH_NUM_VALID_LANGUAGE_SLOTS] = {"japanese", "english", "french", "german", "italian", "spanish", "simplifiedchinese", "korean", "dutch", "portuguese", "russian", "traditionalchinese"};
    static const char* regions[7] = {"japan", "northamerica", "europe", "australia", "china", "korea", "taiwan"};
    static const char* flags[11] = {"visible", "autoboot", "allow3d", "requireeula", "autosave", "extendedbanner", "ratingrequired", "savedata", "recordusage", "nosavebackups", "new3ds"};

    u32 size;
    u8* smdhData = (u8*) read_file(&size, input);
    if (!smdhData) {
        return 1;
    }

    for (int i = 0; i < 12; i++) {
        char16_t shorttitleData[0x80];

        memcpy(shorttitleData, (char16_t*) &smdhData[i * 0x200 + 0x8], 0x80);
        std::string shortTitle = u16_to_string(shorttitleData);

        if (!shortTitle.empty()) {
            char16_t longtitleData[0x100];
            char16_t publisherData[0x80];

            memcpy(longtitleData, (char16_t*) &smdhData[i * 0x200 + 0x8 + 0x80], 0x100);
            memcpy(publisherData, (char16_t*) &smdhData[i * 0x200 + 0x8 + 0x180], 0x80);

            std::string longTitle = u16_to_string(longtitleData);
            std::string publisher = u16_to_string(publisherData);

            config->SetValue(INFO_GROUP, (std::string(languages[i]) + "shorttitle").c_str(), shortTitle.c_str());
            config->SetValue(INFO_GROUP, (std::string(languages[i]) + "longtitle").c_str(), longTitle.c_str());
            config->SetValue(INFO_GROUP, (std::string(languages[i]) + "publisher").c_str(), publisher.c_str());

            // We don't really know what the user would like to have for the default/fallback titles, so we pick English to be safe.
            if (i == 1) {
                config->SetValue(INFO_GROUP, "shorttitle", shortTitle.c_str());
                config->SetValue(INFO_GROUP, "longtitle", longTitle.c_str());
                config->SetValue(INFO_GROUP, "publisher", publisher.c_str());
            }
        }
    }

    if (smdhData[0x2008] > 0) {
        config->SetValue(INFO_GROUP, "cero", std::to_string(smdhData[0x2008]).c_str());
    }
    if (smdhData[0x2009] > 0) {
        config->SetValue(INFO_GROUP, "esrb", std::to_string(smdhData[0x2009]).c_str());
    }
    if (smdhData[0x200B] > 0) {
        config->SetValue(INFO_GROUP, "usk", std::to_string(smdhData[0x200B]).c_str());
    }
    if (smdhData[0x200C] > 0) {
        config->SetValue(INFO_GROUP, "pegigen", std::to_string(smdhData[0x200C]).c_str());
    }
    if (smdhData[0x200E] > 0) {
        config->SetValue(INFO_GROUP, "pegiptr", std::to_string(smdhData[0x200E]).c_str());
    }
    if (smdhData[0x200F] > 0) {
        config->SetValue(INFO_GROUP, "pegibbfc", std::to_string(smdhData[0x200F]).c_str());
    }
    if (smdhData[0x2010] > 0) {
        config->SetValue(INFO_GROUP, "cob", std::to_string(smdhData[0x2010]).c_str());
    }
    if (smdhData[0x2011] > 0) {
        config->SetValue(INFO_GROUP, "grb", std::to_string(smdhData[0x2011]).c_str());
    }
    if (smdhData[0x2012] > 0) {
        config->SetValue(INFO_GROUP, "cgsrr", std::to_string(smdhData[0x2012]).c_str());
    }

    u32 regionbitMask = u32_from_u8_data(smdhData, 0x2018);

    std::string regionsString = regionbitMask == 0x7FFFFFFF ? "regionfree" : "";
    bool hasRegion = false;
    if (regionsString != "regionfree") {
        for (int i = 0; i < 7; i++) {
            u32 bit = 0x1 << i;
            bool isRegion = (regionbitMask & bit) == bit;
            std::string prefix = hasRegion ? "," : "";
            regionsString += isRegion ? prefix + std::string(regions[i]) : "";
            hasRegion = isRegion || hasRegion;
        }
    }

    if (regionsString != "") {
        config->SetValue(INFO_GROUP, "regions", regionsString.c_str());
    }

    u32 matchmakerId = u32_from_u8_data(smdhData, 0x201C);

    if (matchmakerId > 0) {
        config->SetValue(INFO_GROUP, "matchmakerid", std::to_string(matchmakerId).c_str());
    }

    uint64_t mmbid = 0;
    memcpy(&mmbid, &smdhData[0x2020], 0x8);

    if (mmbid > 0) {
        config->SetValue(INFO_GROUP, "matchmakerbitid", std::to_string(mmbid).c_str());
    }

    u32 currentFlags = u32_from_u8_data(smdhData, 0x2028);
    std::string flagsString = "";
    bool hasFlag = false;
    for (int i = 0; i < 11; i++) {
        u32 bit = 0x1 << i;
        bool hasBit = (currentFlags & bit) == bit;
        std::string prefix = hasFlag ? "," : "";
        flagsString += hasBit ? prefix + std::string(flags[i]) : "";
        hasFlag = hasBit || hasFlag;
    }

    config->SetValue(INFO_GROUP, "flags", flagsString.c_str());

    u16 eulaversion = u16_from_u8_data(smdhData, 0x202C);

    config->SetValue(INFO_GROUP, "eulaversion", std::to_string(eulaversion).c_str());

    ufloat bnr;
    bnr.u = u32_from_u8_data(smdhData, 0x2030);

    if (bnr.u > 0) {
        config->SetValue(INFO_GROUP, "optimalbannerframe", std::to_string(bnr.f).c_str());
    }

    u32 cecid = u32_from_u8_data(smdhData, 0x2034);

    if (cecid > 0) {
        config->SetValue(INFO_GROUP, "streetpassid", std::to_string(cecid).c_str());
    }

    extract_image(resourcepath + STR_TOKEN + "smdh.png", SMDH_LARGE_ICON_SIZE, SMDH_LARGE_ICON_SIZE, &smdhData[0x24C0], RGB565);
    extract_image(resourcepath + STR_TOKEN + "smdh-small.png", SMDH_SMALL_ICON_SIZE, SMDH_SMALL_ICON_SIZE, &smdhData[0x2040], RGB565);

    std::string rcPath = getRcPath(resourcepath, output);

    config->SetValue(INFO_GROUP, "icon", (rcPath + "smdh.png").c_str());
    config->SetValue(INFO_GROUP, "smallicon", (rcPath + "smdh-small.png").c_str());

    free(smdhData);
    return 0;
}

static int cmd_extract_bodylz(std::string input, std::string output, std::string resourcepath, std::string bgmpath, CSimpleIniA *config) {
    u32 size;
    u8* compressed = (u8*)read_file(&size, input);
    if (!compressed) {
        return 1;
    }

    u32 decompressed_size = lz11_decompressed_size(compressed);
    u8* data = (u8*)malloc(decompressed_size);
    if (!data) {
        free(compressed);
        return 1;
    }
    lz11_decompress(compressed+4, data, decompressed_size);
    free(compressed);

    std::string rcPath = getRcPath(resourcepath, output);

    std::string bgmfile = apathy::Path(bgmpath).filename();
    bool bgmEnabled = false;
    if (data[0x5] == 0 && !bgmpath.empty()) {
        printf("Warning: bgm file provided, but bgm flag is not enabled. It will be enabled in the extracted rc file.\n");
        bgmEnabled = true;
    } else if (data[0x5] != 0) {
        if (bgmpath.empty()) {
            printf("Warning: bgm file is not provided, but bgm flag is enabled. It will be disabled in the extracted rc file.\n");
        } else {
            bgmEnabled = true;
        }
    }

    if (bgmEnabled) {
        config->SetValue(AUDIO, "bgm", (rcPath + bgmfile).c_str());
    }

    config->SetValue(FRAMES, NULL, NULL);

    static const char* topDrawNames[4] = {"none", "color", "colortexture", "texture"};
    static const char* topFrameNames[4] = {"fastscroll", "single", "invalid", "slowscroll"};

    u8 topdrawtype = data[0xC];
    u8 topframetype = data[0x10];

    u32 extratexture = u32_from_u8_data(data, 0x1C);
    u32 coloroffset = u32_from_u8_data(data, 0x14);
    u32 toptextureoffset = u32_from_u8_data(data, 0x18);
    RGBColors topcolor = rgb_from_u8_data(data, coloroffset);
    u8 topgradient = data[coloroffset + 3];
    u8 topopacity = data[coloroffset + 4];
    u8 topaltopacity = data[coloroffset + 5];
    u8 topgradientcolor = data[coloroffset + 6];

    u32 toptexturewidth = BODYLZ_WIDTH;
    u32 toptextureheight = BODYLZ_HEIGHT;

    PixelFormat topformat = RGB565;

    config->SetValue(FRAMES, "top.type", topDrawNames[topdrawtype]);
    config->SetValue(FRAMES, "top.frame", topFrameNames[topframetype]);

    switch(topdrawtype) {
        case 1:
            config->SetValue(COLORS, "topbgcolor", rgb_to_string(topcolor).c_str());
            config->SetValue(COLORS, "topbgcolor.gradient", std::to_string(topgradient * 100 / 255).c_str());
            config->SetValue(COLORS, "topbgcolor.opacity", std::to_string(topopacity * 100 / 255).c_str());
            break;
        case 2:
            config->SetValue(COLORS, "topbgcolor", rgb_to_string(topcolor).c_str());
            config->SetValue(COLORS, "topbgcolor.gradient", std::to_string(topgradient * 100 / 255).c_str());
            config->SetValue(COLORS, "topbgcolor.opacity", std::to_string(topopacity * 100 / 255).c_str());
            config->SetValue(COLORS, "topbgcolor.altopacity", std::to_string(topaltopacity * 100 / 255).c_str());
            config->SetValue(COLORS, "topbgcolor.gradientcolor", std::to_string(topgradientcolor * 100 / 255).c_str());

            toptexturewidth = BODYLZ_SOLID_COLOR_SIZE;
            toptextureheight = BODYLZ_SOLID_COLOR_SIZE;

            topformat = A8;
            break;
        case 3:
            if (topframetype != 1) {
                toptexturewidth = BODYLZ_WIDTH_WIDE;
            }
            break;
    }

    if (topdrawtype == 2 || topdrawtype == 3) {
        int rotation = topdrawtype == 2 ? -1 : 0;
        extract_image(resourcepath + STR_TOKEN + "toptexture.png", toptexturewidth, toptextureheight, &data[toptextureoffset], topformat, rotation);
        config->SetValue(TEXTURES, "top.image", (rcPath + "toptexture.png").c_str()); 
        if (topdrawtype == 2 && extratexture > 0) {
            extract_image(resourcepath + STR_TOKEN + "topextratexture.png", toptexturewidth, toptextureheight, &data[extratexture], topformat, -1);
            config->SetValue(TEXTURES, "topext.image", (rcPath + "topextratexture.png").c_str());
        }
    }

    static const char* bottomDrawNames[4] = {"none", "color", "invalid", "texture"};
    static const char* bottomFrameNames[5] = {"fastscroll", "single", "pagescroll", "slowscroll", "bouncescroll"};

    u8 bottomdrawtype = data[0x20];
    u8 bottomframetype = data[0x24];

    u32 bottominnercoloroffset = u32_from_u8_data(data, 0x84);
    u32 bottomoutercoloroffset = u32_from_u8_data(data, 0x8C);

    u8 bottominnercolorsenabled = data[0x80];
    u8 bottomoutercolorsenabled = data[0x88];

    u32 bottomtextureoffset = u32_from_u8_data(data, 0x28);

    u32 bottomtexturewidth = BODYLZ_WIDTH;
    u32 bottomtextureheight = BODYLZ_HEIGHT;

    config->SetValue(FRAMES, "bottom.type", bottomDrawNames[bottomdrawtype]);
    config->SetValue(FRAMES, "bottom.frame", bottomFrameNames[bottomframetype]);

    switch(bottomdrawtype) {
        case 1:
            if (bottominnercolorsenabled == 1) {
                RGBColors bottominnerdarkcolor = rgb_from_u8_data(data, bottominnercoloroffset);
                RGBColors bottominnermaincolor = rgb_from_u8_data(data, bottominnercoloroffset + 3);
                RGBColors bottominnerlightcolor = rgb_from_u8_data(data, bottominnercoloroffset + 6);
                RGBAColors bottominnershadowcolor = rgba_from_u8_data(data, bottominnercoloroffset + 9);

                config->SetValue(COLORS, "bottombginnercolor.dark", rgb_to_string(bottominnerdarkcolor).c_str());
                config->SetValue(COLORS, "bottombginnercolor.main", rgb_to_string(bottominnermaincolor).c_str());
                config->SetValue(COLORS, "bottombginnercolor.light", rgb_to_string(bottominnerlightcolor).c_str());
                config->SetValue(COLORS, "bottombginnercolor.shadow", rgba_to_string(bottominnershadowcolor).c_str());
            }
            if (bottomoutercolorsenabled == 1) {
                RGBColors bottomouterdarkcolor = rgb_from_u8_data(data, bottomoutercoloroffset);
                RGBColors bottomoutermaincolor = rgb_from_u8_data(data, bottomoutercoloroffset + 3);
                RGBColors bottomouterlightcolor = rgb_from_u8_data(data, bottomoutercoloroffset + 6);

                config->SetValue(COLORS, "bottombgoutercolor.dark", rgb_to_string(bottomouterdarkcolor).c_str());
                config->SetValue(COLORS, "bottombgoutercolor.main", rgb_to_string(bottomoutermaincolor).c_str());
                config->SetValue(COLORS, "bottombgoutercolor.light", rgb_to_string(bottomouterlightcolor).c_str());
            }
            break;
        case 3:
            if (bottomframetype != 1) {
                bottomtexturewidth = BODYLZ_WIDTH_WIDE;
            }
            break;
    }

    if (bottomdrawtype == 3) {
        extract_image(resourcepath + STR_TOKEN + "bottomtexture.png", bottomtexturewidth, bottomtextureheight, &data[bottomtextureoffset], RGB565);
        config->SetValue(TEXTURES, "bottom.image", (rcPath + "bottomtexture.png").c_str());
    }

    if (data[0x3C] == 1) {
        u32 defaultoffset = u32_from_u8_data(data, 0x40);
        u32 openedoffset = u32_from_u8_data(data, 0x44);

        extract_image(resourcepath + STR_TOKEN + "defaultfolder.png", BODYLZ_FOLDER_WIDTH, BODYLZ_FOLDER_HEIGHT, &data[defaultoffset], RGB888);
        extract_image(resourcepath + STR_TOKEN + "openedfolder.png", BODYLZ_FOLDER_WIDTH, BODYLZ_FOLDER_HEIGHT, &data[openedoffset], RGB888);

        config->SetValue(TEXTURES, "folderclose.image", (rcPath + "defaultfolder.png").c_str());
        config->SetValue(TEXTURES, "folderopen.image", (rcPath + "openedfolder.png").c_str());
    }

    if (data[0x50] == 1) {
        u32 largeoffset = u32_from_u8_data(data, 0x54);
        u32 smalloffset = u32_from_u8_data(data, 0x58);

        extract_image(resourcepath + STR_TOKEN + "largefile.png", BODYLZ_LARGE_FILE_WIDTH, BODYLZ_LARGE_FILE_HEIGHT, &data[largeoffset], RGB888);
        extract_image(resourcepath + STR_TOKEN + "smallfile.png", BODYLZ_SMALL_FILE_WIDTH, BODYLZ_SMALL_FILE_HEIGHT, &data[smalloffset], RGB888);

        config->SetValue(TEXTURES, "filelarge.image", (rcPath + "largefile.png").c_str());
        config->SetValue(TEXTURES, "filesmall.image", (rcPath + "smallfile.png").c_str());
    }

    extractCursorColor(data, config);
    extractFolderColor(data, config);
    extractFileColor(data, config);
    extractArrowButtonColor(data, config);
    extractArrowColor(data, config);
    extractOpenCloseColors(data, config);
    extractGameTextColor(data, config);
    extractFolderBGColor(data, config);
    extractFolderArrowColor(data, config);
    extractBottomCornerButtonColor(data, config);
    extractTopCornerButtonColor(data, config);
    extractDemoTextColor(data, config);

    u32 audio_offset = u32_from_u8_data(data, 0xC0);
    u32 audioenabled = data[0xB8];
    u32 audio_section_size = u32_from_u8_data(data, 0xBC);

    if (audioenabled == 1) {
        u32 current_audio_offset = 0x8;

        static const char *SFX[8] = {"cursor", "launch", "folder", "close", "frame0", "frame1", "frame2", "openlid"};

        for (int i = 0; i < 8; i++) {
            u32 cwav_size= u32_from_u8_data(data, audio_offset + current_audio_offset);
            u32 cwav_offset = audio_offset + current_audio_offset + 0x8;
            // sfx has no header, use the cwav one instead
            if (cwav_size == 0x56415743) {
                cwav_offset = audio_offset + current_audio_offset;
                cwav_size = u32_from_u8_data(data, cwav_offset + 0xC);
            }

            if (cwav_size > 0) {
                if(!write_file(&data[cwav_offset], cwav_size, (resourcepath + STR_TOKEN + SFX[i] + ".bcwav").c_str())) {
                    free(data);
                    return 1;
                }
                config->SetValue(AUDIO, ("sfx." + std::string(SFX[i])).c_str(), (rcPath + SFX[i] + ".bcwav").c_str());
            }

            current_audio_offset += cwav_size + 0x8;
            if (i == 3) {
                current_audio_offset += 0x2C;
            }
            if (audio_section_size <= current_audio_offset) {
                break;
            }
        }
    }
    free(data);
    return 0;
}

static int cmd_convertrgb565(const std::string& input, const std::string& output, bool dither = true) {
    int width, height,n;

    int ok = stbi_info(input.c_str(), &width, &height, &n);

    if(n < 3) {
        printf("converting to RGB565 only makes sense with RGB888 input images. The image you have supplied has fewer than RGB888 colors.\n");
        return 1;
    }
    if(ok != 1) {
        return 1;
    }

    u8* rgb888Data = (u8*) load_image(input.c_str(), width, height);
    if(rgb888Data == NULL) {
        return 1;
    }

    u16 outputData[width * height];

    image_data_to_tiles(outputData, rgb888Data, width, height, RGB565, dither);

    free_image(rgb888Data);

    extract_image(output, width, height, outputData, RGB565);

    printf("Successfully converted RGB888 \"%s\" to RGB565 \"%s\".\n", input.c_str(), output.c_str());
    return 0;
}

int cmd_process_command(int argc, char* argv[]) {
    if(argc < 2) {
        cmd_print_usage(argv[0]);
        return -1;
    }

    char* command = argv[1];
    std::map<std::string, std::string> args = cmd_get_args(argc, argv);
    if (strcmp(command, "-v") == 0 || strcmp(command, "--version") == 0) {
        cmd_print_version();
        return 0;
    } else if(strcmp(command, "makebanner") == 0) {
        const std::string audio = cmd_find_arg(args, "a", "audio", "");
        const std::string cwavFile = cmd_find_arg(args, "ca", "cwavaudio", "");
        const std::string output = cmd_find_arg(args, "o", "output", "");
        if((audio.empty() && cwavFile.empty()) || output.empty()) {
            cmd_missing_args(command);
            return -1;
        }

        static const char* imageShortArgs[CBMD_NUM_CGFXS] = {"i", "eei", "efi", "egi", "eii", "esi", "edi", "epi", "eri", "jji", "uei", "ufi", "usi", "upi"};
        static const char* imageLongArgs[CBMD_NUM_CGFXS] = {"image", "eurenglishimage", "eurfrenchimage", "eurgermanimage", "euritalianimage", "eurspanishimage", "eurdutchimage", "eurportugueseimage", "eurrussianimage", "jpnjapaneseimage", "usaenglishimage", "usafrenchimage", "usaspanishimage", "usaportugueseimage"};
        static const char* cgfxImageShortArgs[CBMD_NUM_CGFXS] = {"ci", "eeci", "efci", "egci", "eici", "esci", "edci", "epci", "erci", "jjci", "ueci", "ufci", "usci", "upci"};
        static const char* cgfxImageLongArgs[CBMD_NUM_CGFXS] = {"cgfximage", "eurenglishcgfximage", "eurfrenchcgfximage", "eurgermancgfximage", "euritaliancgfximage", "eurspanishcgfximage", "eurdutchcgfximage", "eurportuguesecgfximage", "eurrussiancgfximage", "jpnjapanesecgfximage", "usaenglishcgfximage", "usafrenchcgfximage", "usaspanishcgfximage", "usaportuguesecgfximage"};

        std::string images[CBMD_NUM_CGFXS] = {""};
        std::string cgfxFiles[CBMD_NUM_CGFXS] = {""};

        bool found = false;

        for(u32 i = 0; i < CBMD_NUM_CGFXS; i++) {
            images[i] = cmd_find_arg(args, imageShortArgs[i], imageLongArgs[i], "");
            cgfxFiles[i] = cmd_find_arg(args, cgfxImageShortArgs[i], cgfxImageLongArgs[i], "");

            found = found || !images[i].empty() || !cgfxFiles[i].empty();
        }

        if(!found) {
            cmd_missing_args(command);
            return -1;
        }

        return cmd_make_banner(images, audio, cgfxFiles, cwavFile, output);
    } else if(strcmp(command, "makesmdh") == 0) {
        return make_smdh(args, command);
    } else if(strcmp(command, "makecwav") == 0) {
        const std::string input = cmd_find_arg(args, "i", "input", "");
        const std::string output = cmd_find_arg(args, "o", "output", "");
        std::string loop = cmd_find_arg(args, "l", "loop", "false");
        u32 loopStartFrame = (u32) atoi(cmd_find_arg(args, "s", "loopstartframe", "0").c_str());
        u32 loopEndFrame = (u32) atoi(cmd_find_arg(args, "e", "loopendframe", "0").c_str());
        if(input.empty() || output.empty()) {
            cmd_missing_args(command);
            return -1;
        }

        std::transform(loop.begin(), loop.end(), loop.begin(), (int (*)(int)) std::tolower);
        if(loop != "false" && loop != "true") {
            cmd_invalid_arg("loop", command);
            return -1;
        }

        return cmd_make_cwav(input, output, loop == "true", loopStartFrame, loopEndFrame);
    } else if(strcmp(command, "makebodylz") == 0) {
        const std::string config = cmd_find_arg(args, "c", "config", "");
        const std::string output = cmd_find_arg(args, "o", "output", "");
        const std::string nocompress = cmd_find_arg(args, "n", "nocompress", "");
        const std::string nodither = cmd_find_arg(args, "nd", "nodither", "");
        if (config.empty() || output.empty()) {
            cmd_missing_args(command);
            return -1;
        }

        std::string newOutput = output;
        std::string newConfig = config;

        change_directory(newOutput, newConfig);
        return cmd_make_bodylz(newConfig, newOutput, !nocompress.empty(), nodither.empty());
    } else if(strcmp(command, "make3dstheme") == 0) {
        const std::string config = cmd_find_arg(args, "c", "config", "");
        const std::string output = cmd_find_arg(args, "o", "output", "");
        const std::string nozip = cmd_find_arg(args, "n", "nozip", "");
        const std::string nodither = cmd_find_arg(args, "nd", "nodither", "");

        if (config.empty() || output.empty()) {
            cmd_missing_args(command);
            return -1;
        }

        std::string newOutput = output;
        std::string newConfig = config;

        change_directory(newOutput, newConfig);
        return cmd_make_3dstheme(newOutput, newConfig, !nozip.empty(), nodither.empty());
    } else if (strcmp(command, "extractsmdh") == 0) {
        const std::string input = cmd_find_arg(args, "i", "input", "");
        const std::string output = cmd_find_arg(args, "o", "output", "");
        const std::string resourcepath = cmd_find_arg(args, "r", "resources", "");
        if (input.empty() || output.empty() || resourcepath.empty()) {
            cmd_missing_args(command);
            return -1;
        }

        CSimpleIniA config;
        config.SetUnicode();

        SI_Error rc = config.SetValue(INFO_GROUP, NULL, NULL);
        if (rc < 0) {
            return -1;
        }

        if (cmd_extract_smdh(resourcepath, output, input, &config) == 0) {
            rc = config.SaveFile(output.c_str());

            if (rc < 0) {
                printf("Error saving rc file.\n");
                return -1;
            }
            return 0;
        }
        printf("Failed to extract smdh.\n");
        return -1;
    } else if(strcmp(command, "extractbodylz") == 0) {
        const std::string input = cmd_find_arg(args, "i", "input", "");
        const std::string output = cmd_find_arg(args, "o", "output", "");
        const std::string resources = cmd_find_arg(args, "r", "resources", "");
        const std::string bgmpath = cmd_find_arg(args, "b", "bgm", "");
        if (input.empty() || output.empty() || resources.empty()) {
            cmd_missing_args(command);
            return -1;
        }

        CSimpleIniA config;
        config.SetUnicode();

        SI_Error rc = config.SetValue(FRAMES, NULL, NULL);
        if (rc < 0) {
            return -1;
        }

        if(cmd_extract_bodylz(input, output, resources, bgmpath, &config) == 0) {
            rc = config.SaveFile(output.c_str());

            if (rc < 0) {
                printf("Error saving rc file.\n");
                return -1;
            }
            return 0;
        }
        printf("Failed to extract body_lz.bin.\n");
        return -1;
    } else if(strcmp(command, "extract3dstheme") == 0) {
        const std::string input = cmd_find_arg(args, "i", "input", "");
        const std::string output = cmd_find_arg(args, "o", "output", "");
        const std::string resources = cmd_find_arg(args, "r", "resources", "");
        const std::string folder = cmd_find_arg(args, "f", "folder", "");
        if (input.empty() || output.empty() || resources.empty()) {
            cmd_missing_args(command);
            return -1;
        }

        bool isfolder = !folder.empty();

        // These files might not be case sensitive
        std::string canonicalSmdhFileName;
        std::string canonicalBodylzFileName;
        std::string canonicalBgmFileName;
        std::string inputPath = isfolder ? input : resources;

        if (isfolder) {
            DIR *dir;
            struct dirent *ent;
            if ((dir = opendir(input.c_str())) != NULL) {
                while((ent = readdir(dir)) != NULL) {
                    char * filename = ent->d_name;
                    if (strcasecmp(filename, "bgm.bcstm") == 0) {
                        canonicalBgmFileName = filename;
                    } else if (strcasecmp(filename, "info.smdh") == 0) {
                        canonicalSmdhFileName = filename;
                    } else if (strcasecmp(filename, "body_lz.bin") == 0) {
                        canonicalBodylzFileName = filename;
                    }
                }
                closedir(dir);
            }
            if (canonicalBodylzFileName.empty()) {
                printf("The folder you have selected does not contain a 3DS theme file.");
                return -1;
            }
            if (!canonicalBgmFileName.empty()) {
                u32 bgmSize;
                void* bgm = read_file(&bgmSize, input + STR_TOKEN + canonicalBgmFileName);
                write_file(bgm, bgmSize, resources + STR_TOKEN + canonicalBgmFileName);
            }
        } else {
            mz_bool status;

            mz_zip_archive archive;
            memset(&archive, 0, sizeof(archive));
            status = mz_zip_reader_init_file(&archive, input.c_str(), 0);

            if (!status) {
                mz_zip_reader_end(&archive);
                printf("Error reading zip archive.\n");
                return -1;
            }
            int fileCount = (int)mz_zip_reader_get_num_files(&archive);
            if (fileCount == 0)
            {
                mz_zip_reader_end(&archive);
                printf("Error: No files found in zip archive.\n");
                return -1;
            }
            mz_zip_archive_file_stat file_stat;
            status = mz_zip_reader_file_stat(&archive, 0, &file_stat);
            if (!status) {
                mz_zip_reader_end(&archive);
                printf("Error: Unable to obtain zip archive file information.\n");
                return -1;
            }

            for (int i = 0; i < fileCount; i++) {
                status = mz_zip_reader_file_stat(&archive, i, &file_stat);
                if (!status) continue;
                status = mz_zip_reader_is_file_a_directory(&archive, i);
                if (status) continue;
                std::string fileName = file_stat.m_filename;

                if (strcasecmp(fileName.c_str(), "info.smdh") == 0 || strcasecmp(fileName.c_str(), "body_lz.bin") == 0 || strcasecmp(fileName.c_str(), "bgm.bcstm") == 0) {
                    status = mz_zip_reader_extract_to_file(&archive, i, (resources + STR_TOKEN + fileName).c_str(), 0);
                    if (!status) {
                        printf("Failed to extract file %s to %s.\n", fileName.c_str(), resources.c_str());
                        return -1;
                    }
                    if (strcasecmp(fileName.c_str(), "bgm.bcstm") == 0) {
                        canonicalBgmFileName = fileName;
                    } else if (strcasecmp(fileName.c_str(), "info.smdh") == 0) {
                        canonicalSmdhFileName = fileName;
                    } else {
                        canonicalBodylzFileName = fileName;
                    }
                }
            }

            mz_zip_reader_end(&archive);
        }

        CSimpleIniA config;
        config.SetUnicode();

        SI_Error rc = config.SetValue(FRAMES, NULL, NULL);
        if (rc < 0) {
            return -1;
        }

        if (canonicalSmdhFileName.empty()) {
            printf("Warning: Input path does not contain a SMDH file. The theme can still be extracted, but will contain no metadata.\n");
        } else {
            if (cmd_extract_smdh(resources, output, inputPath + STR_TOKEN + canonicalSmdhFileName, &config) != 0) {
                printf("Failed to extract smdh.\n");
                return -1;
            }
        }

        std::string bgmpath = canonicalBgmFileName.empty() ? "" : resources + STR_TOKEN + canonicalBgmFileName;
        if (cmd_extract_bodylz(inputPath + STR_TOKEN + canonicalBodylzFileName, output, resources, bgmpath, &config) != 0) {
            printf("Failed to extract body_lz.bin.\n");
            return -1;
        }

        rc = config.SaveFile(output.c_str());

        if (rc < 0) {
            printf("Error saving rc file.\n");
            return -1;
        }
        return 0;
    } else if(strcmp(command, "convertrgb565") == 0) {
        const std::string input = cmd_find_arg(args, "i", "input", "");
        const std::string output = cmd_find_arg(args, "o", "output", "");
        const std::string nodither = cmd_find_arg(args, "nd", "nodither", "");
        if(input.empty() || output.empty()) {
            cmd_missing_args(command);
            return -1;
        }

        return cmd_convertrgb565(input, output, nodither.empty());
    } else if(strcmp(command, "lz11") == 0) {
        const std::string input = cmd_find_arg(args, "i", "input", "");
        const std::string output = cmd_find_arg(args, "o", "output", "");
        if(input.empty() || output.empty()) {
            cmd_missing_args(command);
            return -1;
        }

        return cmd_lz11(input, output);
    } else if (strcmp(command, "decompresslz11") == 0) {
        const std::string input = cmd_find_arg(args, "i", "input", "");
        const std::string output = cmd_find_arg(args, "o", "output", "");
        if(input.empty() || output.empty()) {
            cmd_missing_args(command);
            return -1;
        }

        return cmd_decompresslz11(input, output);
    } else {
        cmd_invalid_command(command);
        return -1;
    }
}
