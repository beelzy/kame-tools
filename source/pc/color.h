#ifndef COLOR_H
#define COLOR_H

#include "../types.h"
#include <math.h>

typedef enum {
    RGB565,
    RGB888,
    RGBA4444,
    RGBA8888,
    A8
} PixelFormat;

/* Dither Tresshold for Red Channel */
static const u8 dither_tresshold_r[64] = {
  1, 7, 3, 5, 0, 8, 2, 6,
  7, 1, 5, 3, 8, 0, 6, 2,
  3, 5, 0, 8, 2, 6, 1, 7,
  5, 3, 8, 0, 6, 2, 7, 1,

  0, 8, 2, 6, 1, 7, 3, 5,
  8, 0, 6, 2, 7, 1, 5, 3,
  2, 6, 1, 7, 3, 5, 0, 8,
  6, 2, 7, 1, 5, 3, 8, 0
};

/* Dither Tresshold for Green Channel */
static const u8 dither_tresshold_g[64] = {
  1, 3, 2, 2, 3, 1, 2, 2,
  2, 2, 0, 4, 2, 2, 4, 0,
  3, 1, 2, 2, 1, 3, 2, 2,
  2, 2, 4, 0, 2, 2, 0, 4,

  1, 3, 2, 2, 3, 1, 2, 2,
  2, 2, 0, 4, 2, 2, 4, 0,
  3, 1, 2, 2, 1, 3, 2, 2,
  2, 2, 4, 0, 2, 2, 0, 4
};

/* Dither Tresshold for Blue Channel */
static const u8 dither_tresshold_b[64] = {
  5, 3, 8, 0, 6, 2, 7, 1,
  3, 5, 0, 8, 2, 6, 1, 7,
  8, 0, 6, 2, 7, 1, 5, 3,
  0, 8, 2, 6, 1, 7, 3, 5,

  6, 2, 7, 1, 5, 3, 8, 0,
  2, 6, 1, 7, 3, 5, 0, 8,
  7, 1, 5, 3, 8, 0, 6, 2,
  1, 7, 3, 5, 0, 8, 2, 6
};

/* Get 16bit closest color */
u8 closest_rb(u8 c) {
  return (c >> 3 << 3); /* red & blue */
}
u8 closest_g(u8 c) {
  return (c >> 2 << 2); /* green */
}

/* RGB565 */
u16 RGB16Bit(u8 r, u8 g, u8 b) {
  return ((u16)((r>>3)<<11)|((g>>2)<<5)|(b>>3));
}

/* Dithering by individual subpixel */
u16 dither_xy(
  int x,
  int y,
  u8 r,
  u8 g,
  u8 b
){
  /* Get Tresshold Index */
  u8 tresshold_id = ((y & 7) << 3) + (x & 7);

  r = closest_rb(
          std::min(r + dither_tresshold_r[tresshold_id], 0xff)
       );
  g = closest_g(
          std::min(g + dither_tresshold_g[tresshold_id], 0xff)
       );
  b = closest_rb(
          std::min(b + dither_tresshold_b[tresshold_id], 0xff)
       );
  return RGB16Bit(r, g, b);
}

// Dithering Pixel from 32/24bit RGB
u16 dither_color_xy(int x, int y, u8* col) {
  return dither_xy(x, y, col[0], col[1], col[2]);
}

double sRGB_to_linear(double x) {
        if (x < 0.04045) return x / 12.92;
        return pow((x + 0.055) / 1.055, 2.4);
}

double linear_to_sRGB(double y) {
        if (y <= 0.0031308) return 12.92 * y;
        return 1.055 * pow(y, 1 / 2.4) - 0.055;
}

u32 formatsize(PixelFormat format) {
    switch(format) {
        case RGB888:
            return sizeof(RGBColors);
            break;
        case RGB565:
            return sizeof(u16);
            break;
        case A8:
            return sizeof(u8);
            break;
        default:
            return sizeof(u32);
            break;
    }
}

void rotate(u8 *source, u8 *output, u32 width, u32 height, PixelFormat format = RGBA8888) {
    u32 colorsize = formatsize(format);

    for(u32 y = 0; y < height; y++) {
        for (u32 x = 0; x < width; x++) {
            for (u8 i = 0; i < colorsize; i++) {
                ((u8*) output)[(x * height + (height - y - 1)) * colorsize + i] = ((u8*) source)[(y * width + x) * colorsize + i];
            }
        }
    }
}

void inverse_rotate(u8 *source, u8 *output, u32 width, u32 height, PixelFormat format = RGBA8888) {
    u32 colorsize = formatsize(format);

    for(u32 y = 0; y < height; y++) {
        for (u32 x = 0; x < width; x++) {
            for (u8 i = 0; i < colorsize; i++) {
                ((u8*) output)[(y * width + x) * colorsize + i] = ((u8*) source)[(x * height + (height - y - 1)) * colorsize + i];
            }
        }
    }
}

#endif
