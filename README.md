# Kame Tools

A fork of bannertool, which includes the ability to create `body_lz.bin` files necessary to create 3DS themes.

Since there are tons of options supplied in 3DS themes, you supply options to kame-tools through a configuration file instead of directly in the command line. You can find an example in `example.rc`.

## Building

Building kame-tools is pretty straightforward. Obtain the git submodules first:

`git submodule init`
`git submodule update`

Then build it:

`make`

If you are running aarch64, go into the `buildtools` folder and apply the patch before the build step so that `buildtools` works:

`git apply aarch64.patch`

The resulting binary will be in the `output` folder.

## BGM and SFX Support

This tool supports including bgms in the .bcstm format as well as sfxs in the .bcwav format, but does not include any converters. For a cross-platform way to do this, there is now a fork of [rstmcpp](https://gitlab.com/beelzy/rstmcpp).
We still recommend the [Audacity fork](https://github.com/jackoalan/audacity) for SFX editing, since it does bcwav conversions as well, and you'll probably need to edit the original sound files anyways.

## Quick Usage

Just run `kame-tools make3dstheme -c [path/to/config/file] -o [path/to/output]`. Ideally, the final archive is a .zip file.

You can now also extract images and audio from theme archives. Run `kame-tools extract3dstheme -i [path/to/zip] -o [path/to/output/rc] -r [path/to/output/resources]`.

## GUI Frontend

There is now a GUI frontend that uses kame-tools for a nicer experience. It's still a work in progress, but you can at least use it to make themes.

[Kame Editor](https://gitlab.com/beelzy/kame-editor)
